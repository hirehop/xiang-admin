package com.i.mocule.test.controller;

import com.i.codegenerator.domain.CodeGeneratorDTO;
import com.i.codegenerator.service.CodeGeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestCodeGenerate {

    @Autowired
    CodeGeneratorService codeGeneratorService;

    @GetMapping("/testCodeGenerate")
    public void test1(){
        try {
            CodeGeneratorDTO xiang = CodeGeneratorDTO.builder()
                    .author("X" )
                    .tableName("t_employee" )
                    .tablePrefix("t_" )
                    // 默认值见CodeGenerateService.java
                    .basePackage("net.i.xiangadmin.test" )
                    .modulePackage("aaa" )
                    .build();
            // 上述配置生成的包： net.i.xiangadmin.test.module.aaa.controller;
            codeGeneratorService.codeGenerator(xiang);
        }catch (Exception e){
            log.error(e.getMessage());
        }
    }
}

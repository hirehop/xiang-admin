package com.i.codegenerator.domain;

import lombok.Builder;
import lombok.Data;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
@Builder
public class QueryFieldVO {

    private String fieldName;

    private String columnName;

    private String columnDesc;

    private String fieldType;

    private String sqlOperate;

}

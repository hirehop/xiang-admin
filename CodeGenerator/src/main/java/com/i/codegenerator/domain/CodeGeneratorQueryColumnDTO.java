package com.i.codegenerator.domain;

import lombok.Builder;
import lombok.Data;
import com.i.codegenerator.constant.SqlOperateTypeEnum;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
@Builder
public class CodeGeneratorQueryColumnDTO {

    /**
     * 生成查询方法的查询列名
     */
    private String columnName;

    /**
     * 此列的查询动作
     */
    private SqlOperateTypeEnum sqlOperate;
}

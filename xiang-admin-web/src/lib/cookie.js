// js-cookie库的基本使用参考这篇文档：https://gitee.com/hirehop/javascript-improving/tree/master/js-cookie
// 1.定义token_key
// 2.从配置文件中读取默认cookieExpires
// 3.setToken + getToken + clearToken

import Cookies from 'js-cookie';
import config from '@/config';

export const TOKEN_KEY = 'token';
const { cookieExpires } = config;

export default {
  setToken: token => {
    Cookies.set(TOKEN_KEY, token, {
      expires: cookieExpires || 7
    });
  },
  getToken: () => {
    const token = Cookies.get(TOKEN_KEY);
    if (token) {
      return token;
    } else {
      return null;
    }
  },
  clearToken: () => {
    Cookies.remove(TOKEN_KEY);
  }
};

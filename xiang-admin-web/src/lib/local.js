/* 对H5中localStorage的封装，用于缓存用户信息及权限 */

export const localSave = (key, value) => {
  localStorage.setItem(key, value);
};

export const localRead = key => {
  return localStorage.getItem(key) || '';
};

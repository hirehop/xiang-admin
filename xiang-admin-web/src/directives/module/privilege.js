/* Vue自定义指令官方文档：https://cn.vuejs.org/v2/guide/custom-directive.html */

// 页面内按钮过滤
import store from '@/store/index';
export default {
  // 当被绑定的元素插入到 DOM 中时
  inserted: function (el, binding, vnode) {
    // 获取当前路由name
    let routeName = vnode.context.$route.name;
    // 超级管理员
    if (store.state.user.userLoginInfo.isSuperMan) {
      return true;
    }
    // 获取功能点权限
    let functionList = store.state.user.privilegeFunctionPointsMap.get(routeName);
    // 有权限
    if (functionList && functionList.includes(binding.value)) {

    } else {
      el.parentNode.removeChild(el);
    }
  }
};

import privilege from './module/privilege';

const directives = {
  privilege
};

export default directives;

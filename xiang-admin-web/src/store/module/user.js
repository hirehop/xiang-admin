import cookie from '@/lib/cookie.js';
import { loginApi } from '@/api/login';
import { localSave, localRead } from '@/lib/local';
import { getType } from '@/lib/util';
import { PRIVILEGE_TYPE_ENUM } from '@/constants/privilege';

// 本地读取'路有权限'
const localReadRouterPrivilege = () => {
  let map = new Map();
  // 从localStorage中读取参数信息：userRouterPrivilege（用户路由权限）
  // json字符串
  let userRouterPrivilegeString = localRead('userRouterPrivilege');
  if (userRouterPrivilegeString) {
    let privilegeList = JSON.parse(userRouterPrivilegeString);
    if (privilegeList) {
      for (const path of privilegeList) {
        // 键为第二个字符
        let key = path.substr(1, 1);
        // 取path的第二个字母（按照规范，路径必须以/开头，这里取路径首字母）为键，建立路由权限映射表
        let pathArray = map.get(key);
        if (pathArray) {
          pathArray.push(path);
        } else {
          pathArray = [];
          pathArray.push(path);
          map.set(key, pathArray);
        }
      }
    }
  }
  return map;
};

export default {
  state: {
    token: cookie.getToken(),
    // session信息
    userLoginInfo: {},
    isUpdatePrivilege: false,
    privilegeFunctionPointsMap: new Map(),
    // 【菜单key权限集合】，用于左侧是否有菜单权限判断
    privilegeMenuKeyList: [],
    /**
     * key为 router path的首字母，value为集合
     * 这样做是为了提高查询效率，用于vue-router拦截判断path
     */
    privilegeRouterPathMap: localReadRouterPrivilege()

  },
  mutations: {
    // 设置token（使用js-cookie开源组件）
    setToken (state, token) {
      state.token = token;
      cookie.setToken(token);
    },
    // 保存用户登录信息（代码中获取用户ID不需要使用loginApi，直接从store中获取即可）
    //   store.state.user.userLoginInfo
    setUserLoginInfo (state, userLoginInfo) {
      state.userLoginInfo = userLoginInfo;
      localSave('userLoginInfo', JSON.stringify(userLoginInfo));
    },
    // 设置用户权限
    // 参数二：当前用户的所有权限
    setUserPrivilege (state, privilegeList) {
      state.isUpdatePrivilege = true;
      // 路由路径列表
      let routerPathArray = [];
      for (const privilege of privilegeList) {
        // 是菜单权限
        if (privilege.type === PRIVILEGE_TYPE_ENUM.MENU.value) {
          state.privilegeMenuKeyList.push(privilege.key);
          if (privilege.url) {
            routerPathArray.push(privilege.url);
            // 去掉/之后，获取首字母
            let key = privilege.url.substr(1, 1);
            let pathArray = state.privilegeRouterPathMap.get(key);
            if (pathArray) {
              pathArray.push(privilege.url);
            } else {
              pathArray = [];
              pathArray.push(privilege.url);
              state.privilegeRouterPathMap.set(key, pathArray);
            }
          }
        }
        // 如果是功能点
        if (privilege.type === PRIVILEGE_TYPE_ENUM.POINTS.value) {
          if (privilege.parentKey) {
            let pointArray = state.privilegeFunctionPointsMap.get(privilege.parentKey);
            if (pointArray) {
              pointArray.push(privilege.key);
            } else {
              pointArray = [];
              pointArray.push(privilege.key);
              state.privilegeFunctionPointsMap.set(privilege.parentKey, pointArray);
            }
          }
        }
      }
      localSave('userRouterPrivilege', JSON.stringify(routerPathArray));
    }
  },
  getters: {
    // 用户功能点权限
    userFuncPrivilegeInfo: () => localRead('funcPrivilegeInfo'),
    // 用户菜单权限
    userMenuPrivilege: state => state.userLoginInfo.privilegeList
  },
  actions: {
    // 登录
    // 参数一从传入的参数中解构出commit属性
    handleLogin ({ commit }, params) {
      params.loginName = params.loginName.trim();
      return new Promise((resolve, reject) => {
        loginApi
          .login(params)
          .then(res => {
            localStorage.clear();
            const data = res.data;
            commit('setToken', data.xaccessToken);
            // 保存用户登录
            commit('setUserLoginInfo', data);
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });
    }
  }
};

export const FILE_TYPE = {
  LOCAL: {
    value: 1,
    desc: '本地文件服务'
  },
};
export const SERVICE_TYPE = {
  BACK_USER: {
    value: '1',
    desc: '用户后台'
  }
};
export default {
  FILE_TYPE,
  SERVICE_TYPE
};

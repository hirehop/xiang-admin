import login from './login.js';
import file from './file.js';
import privilege from './privilege';
export default {
  ...login,
  ...file,
  ...privilege
};

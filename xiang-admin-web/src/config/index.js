export default {

  // 配置显示在浏览器标签的title
  title: 'Xiang-Admin',

  // 默认打开的首页的路由name值
  homeName: 'Home',

  // token在Cookie中存储的天数，默认1天
  cookieExpires: 1,

  // 是否使用国际化，默认为false，如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
  // 用来在菜单中显示文字
  useI18n: false,

  // api请求基础路径
  baseUrl: {
    // npm install --save @types/node
    // 从配置文件中读取环境变量
    // 官方文档：VueCLI => https://cli.vuejs.org/zh/guide/mode-and-env.html#%E6%A8%A1%E5%BC%8F
    apiUrl: process.env.VUE_APP_URL,
    webSocketUrl: process.env.VUE_APP_SOCKET_URL
  }
};

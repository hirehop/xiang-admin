// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import ViewUI from 'view-design';
import i18n from '@/locale';
import config from '@/config';
import importDirective from '@/directives';
import JsonViewer from 'vue-json-viewer';
import _ from 'lodash';
import './themes/index.less';
// 导入阿里巴巴矢量图标库
import '@/assets/icons/iconfont.css';
import 'slick-carousel/slick/slick.css';
import { Decimal } from 'decimal.js';

// 导入前端枚举插件
import Enum from '@/plugin/vue-enum';
import enumInfo from '@/constants';

// 处理table操作按钮
import tableAction from './lib/table-action';

// 时间组件
import moment from 'moment';

// e-guide-layer组件
import 'e-guide-layer/dist/e-guide-layer.css';
import eGuideLayer from 'e-guide-layer';

Vue.use(eGuideLayer);
Vue.prototype.$tableAction = tableAction;
Vue.use(Enum, { enumInfo });
Vue.use(ViewUI, {
  i18n: (key, value) => i18n.t(key, value)
});
Vue.use(JsonViewer);

// eslint-disable-next-line no-extend-native
Number.prototype.toFixed = function (length) {
  let x = new Decimal(this);
  return x.toFixed(length);
};

// 时间处理
moment.locale('zh-cn'); // 设置语言 或 moment.lang('zh-cn');
Vue.prototype.$moment = moment;// 赋值使用

/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false;

/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config;

/**
 * 注册指令
 */
importDirective(Vue);

window._ = _;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
});

/* @/lib/menu-func中对路由进行了大量调度与操作 */
import Main from '@/components/main';

import { course } from './course';
import { file } from './file';

// 业务功能，新增的路由界面在这定义
export const business = [
  {
    path: '/business',
    name: 'Business',
    component: Main,
    meta: {
      title: '业务功能',
      // 控制第一个菜单的显示
      topMenu:true,
      icon: 'icon iconfont iconyoujianguanli'
    },
    children: [
      ...course,
      ...file,
    ]
  },
];

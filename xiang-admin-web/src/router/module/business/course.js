import Main from '_c/main';
// 课程设置
export const course = [
  {
    // name将会作为权限表的key，path作为url
    //   路由分为两种：菜单+功能点
    path: '/course',
    name: 'Course',
    component: Main,
    meta: {
      title: '课程管理',
      icon: 'icon iconfont iconyoujianguanli'
    },
    children: [
      {
        path: '/course/course-A',
        name: 'CourseA',
        meta: {
          title: '课程信息',
          privilege: [
            { title: '查询', name: 'course-info-query' },
          ]
        },
        component: () => import('@/views/business/course/course-info.vue')
      },
      {
        path: '/course/course-A1',
        name: 'CourseA1',
        meta: {
          title: '课程管理',
          privilege: [
            { title: '增加', name: 'course-manage-add' },
            { title: '删除', name: 'course-manage-delete' },
            { title: '修改', name: 'course-manage-update' },
            { title: '查询', name: 'course-manage-query' },
          ]
        },
        component: () => import('@/views/business/course/course-arrange.vue')
      },
      {
        path: '/course/course-B',
        name: 'CourseB',
        meta: {
          title: '考试信息',
          privilege: [
            { title: '查询', name: 'exam-info-query' },
          ]
        },
        component: () => import('@/views/business/course/exam-info.vue')
      },
      {
        path: '/course/course-C',
        name: 'CourseC',
        meta: {
          title: '评教分数',
          privilege: [
            { title: '修改', name: 'score-manage-update' },
            { title: '查询', name: 'score-manage-query' },
          ]
        },
        component: () => import('@/views/business/course/score-info.vue')
      }
    ]
  }
];

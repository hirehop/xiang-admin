import Main from '_c/main';
// 文件服务
export const file = [
  {
    path: '/file',
    name: 'File',
    component: Main,
    meta: {
      title: '教学资料',
      icon: 'ios-cloud-upload'
    },
    children: [
      // {
      //   path: '/file/file-list',
      //   name: 'FileList',
      //   meta: {
      //     title: '教学文件',
      //     icon: '',
      //     privilege: [
      //       { title: '查询', name: 'file-filePage-query' },
      //       { title: '上传', name: 'file-filePage-upload' },
      //       { title: '下载', name: 'file-filePage-download' },
      //       { title: '删除', name: 'file-filePage-delete' }
      //     ]
      //   },
      //   component: () => import('@/views/business/file/file-list.vue')
      // },
      {
        path: '/file/file-syllabus-list',
        name: 'FileSyllabusList',
        meta: {
          title: '教学大纲',
          icon: '',
          privilege: [
            { title: '查询', name: 'file-syllabus-query' },
            { title: '上传', name: 'file-syllabus-upload' },
            { title: '下载', name: 'file-syllabus-download' },
            { title: '删除', name: 'file-syllabus-delete' }
          ]
        },
        component: () => import('@/views/business/file/file-syllabus.vue')
      },
      {
        path: '/file/file-calendar-list',
        name: 'FileCalendarList',
        meta: {
          title: '教学日历',
          icon: '',
          privilege: [
            { title: '查询', name: 'file-calendar-query' },
            { title: '上传', name: 'file-calendar-upload' },
            { title: '下载', name: 'file-calendar-download' },
            { title: '删除', name: 'file-calendar-delete' }
          ]
        },
        component: () => import('@/views/business/file/file-calendar.vue')
      },
      {
        path: '/file/file-summary-list',
        name: 'FileSummaryList',
        meta: {
          title: '教学小结',
          icon: '',
          privilege: [
            { title: '查询', name: 'file-summary-query' },
            { title: '上传', name: 'file-summary-upload' },
            { title: '下载', name: 'file-summary-download' },
            { title: '删除', name: 'file-summary-delete' }
          ]
        },
        component: () => import('@/views/business/file/file-summary.vue')
      }
    ]
  }
];

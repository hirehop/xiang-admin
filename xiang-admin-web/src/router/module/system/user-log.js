import Main from '@/components/main';
// 用户日志
export const userLog = [
  {
    path: '/user-log',
    name: 'UserLog',
    component: Main,
    meta: {
      title: '用户日志',
      icon: 'ios-paper-outline'
    },
    children: [
      {
        path: '/user-log/user-login-log',
        name: 'UserLoginLog',
        meta: {
          title: '用户登录日志',
          icon: 'ios-paper-outline',
          privilege: [
            { title: '查询', name: 'user-login-log-search' },
            { title: '删除', name: 'user-login-log-delete' }
          ]
        },
        component: () => import('@/views/system/user-log/user-login-log.vue')
      }
    ]
  }
];

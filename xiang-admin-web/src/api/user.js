import { postAxios } from '@/lib/http';

export const login = ({ userName, password }) => {
  const data = {
    userName,
    password
  };
  return postAxios('login', data);
};

export const logout = (token) => {
  return postAxios('logout', {});
};

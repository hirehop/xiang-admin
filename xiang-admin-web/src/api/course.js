import { postAxios, getAxios } from '@/lib/http';
export const courseApi = {
  // 分页查询
  getCourseListByPage : (data) =>{
    return postAxios('/tCourse/page/query', data);
  },
  // 删除
  deleteCourse: id => {
    return getAxios('/tCourse/delete?id=' + id);
  },
  // 新增
  addCourse : data =>{
    return postAxios('/tCourse/add', data);
  },
  // 新增
  updateCourse : data =>{
    return postAxios('/tCourse/update', data);
  }
};

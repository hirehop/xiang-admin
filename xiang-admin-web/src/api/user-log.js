// 用户日志API
import {
  postAxios,
  getAxios
} from '@/lib/http.js';
export const userLogApi = {
  // 查询用户登录日志
  getUserLoginLogPage: (data) => {
    return postAxios('/userLoginLog/page/query', data);
  },
  // 删除用户登录日志
  deleteUserLoginLog: (data) => {
    return getAxios('/userLoginLog/delete/' + data);
  }
};

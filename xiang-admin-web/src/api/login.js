import { postAxios, getAxios } from '@/lib/http';
export const loginApi = {
  // 登录
  // 返回LoginDetailDTO，内部包含基本信息+token+超管标记+权限列表等
  login: data => {
    return postAxios('/session/login', data);
  },
  // 登出
  logout: (token) => {
    return getAxios(`/session/logOut?x-access-token=${token}`);
  },
  // 根据token获取session
  getSession: () => {
    return getAxios('/session/get');
  },
  // 获取验证码
  getVerificationCode: () => {
    return getAxios('/session/verificationCode');
  }
};

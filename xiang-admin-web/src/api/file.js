import { postAxios, getAxios, getDownloadAxios} from '@/lib/http';
import config from '@/config';
const baseUrl = config.baseUrl.apiUrl;

/**
 * [ 指定接口路径：/api/file/XXX ]
 * [ 查询、上传、下载、保存 ]
 */
export const fileApi = {
  // 系统文件查询
  queryFileList: data => {
    return postAxios('/api/file/query', data);
  },
  // 文件上传
  fileUpload: (type, data) => {
    return postAxios('/api/file/localUpload/' + type, data);
  },
  // 系统文件下载（ID）
  downLoadFile: id => {
    return getDownloadAxios('/api/file/downLoad?id=' + id);
  },
  // 文件保存
  addFile: data => {
    return postAxios('/api/file/save', data);
  },
  // 文件删除
  deleteFile: id => {
    return getAxios('/api/file/delete?id=' + id);
  },
  // 上传路径：本地
  fileUploadLocalUrl: baseUrl + 'api/file/localUpload/',
};

const path = require('path');
const resolve = dir => {
  return path.join(__dirname, dir);
};
// 项目部署基础
// 默认情况下，我们假设你的应用将被部署在域的根目录下,
// 例如：https://www.my-app.com/
// 默认：'/'
// 如果您的应用程序部署在子路径中，则需要在这指定子路径
// 例如：https://www.foobar.com/my-app/
// 需要将它改为'/my-app/'
const publicPath = process.env.NODE_ENV === 'production' ? '/' : '/';
// 调整内部webpack配置
// see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
// 如果你不需要使用eslint，把lintOnSave设为false即可
const lintOnSave = process.env.NODE_ENV === 'production';

module.exports = {
  publicPath,
  lintOnSave,
  chainWebpack: config => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('_c', resolve('src/components'));
  }
};

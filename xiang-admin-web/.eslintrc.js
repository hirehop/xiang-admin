module.exports = {
    root: true,
    env: {
        // 不加此项，vue文件中使用require语法会异常
        node: true
    },
    extends: [
        "plugin:vue/essential",
        "eslint:recommended"
    ],
    parserOptions: {
        "parser": "babel-eslint"
    },
    rules: {
        "no-console": "off",  // 允许代码中使用console
        'no-multiple-empty-lines': [1, { 'max': 2 }], //空行最多不能超过2行
        /* Vue使用外部组件库，取消报错 */
        'vue/no-parsing-error': [2, {
            'x-invalid-end-tag': false
        }],
        'no-const-assign': 2, //禁止修改const声明的变量
        'no-unused-vars': [0, { //禁止声明变量后却不使用
            // 允许声明未使用变量
            'vars': 'all',
            // 参数不检查
            'args': 'none'
        }],
        // 'quotes': [2, 'single'], //单引号
        'indent': ['error', 2],  //缩进量
        'no-var': 2,             //禁用var，用let和const代替
        'camelcase': 2,          //强制驼峰法命名
        'eqeqeq': 1,             //要求使用 === 和 !== 代替 == 和 != 操作符
        'no-eq-null': 2,         //禁止对null使用==或!=运算符
        'semi': [2, 'always']    //强制分号
    }
};

## 启动讲解

---

1. 安装依赖：

- `npm install` 官网下载且会自动生成`package.json`文件
- `cnpm install` 阿里镜像下载，不会生成`package.json`文件

2. 运行本地环境

`npm run local`





## 后端响应拦截器

---

1. 前后端请求使用开源Axios库

2. 前端在Axios中配置响应拦截器，旨在对后端异常请求统一过滤输出错误信息

   ```javascript
   let axios = Axios.create({
     baseURL: baseUrl,
     timeout: 30000,
     headers: {
       'Content-Type': 'application/json; charset=utf-8'
     }
   });
   
   axios.interceptors.response.use( res => {
       // ...
   } )
   ```

   1. 再封装Axios，将其放在根目录子级lib下

   2. 需要时import引用即可

      ```javascript
      import { postAxios, getAxios } from '@/lib/http';
      ```

      

   


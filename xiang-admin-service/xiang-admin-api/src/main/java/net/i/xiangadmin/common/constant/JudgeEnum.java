package net.i.xiangadmin.common.constant;

import java.util.Arrays;
import java.util.Optional;

/**
 *
 * [ 是与否 枚举类]
 *
 * @version 1.0
 * @since JDK1.8
 */
public enum JudgeEnum implements BaseEnum {

    NO(0, "否"),

    YES(1, "是");

    private Integer value;
    private String desc;

    JudgeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }
    @Override
    public String getDesc() {
        return desc;
    }

    /**
     * 将value值封装为JudgeEnum
     * @param value 需要被封装为枚举类型的值
     * @return 封装后的枚举类型
     */
    public static JudgeEnum valueOf(Integer value) {
        JudgeEnum[] values = JudgeEnum.values();
        Optional<JudgeEnum> first = Arrays.stream(values).filter(e -> e.getValue().equals(value)).findFirst();
        return !first.isPresent() ? null : first.get();
    }

    /**
     * [ 判断值是否存在于当前枚举类中 ]
     * @param value
     * @return
     */
    public static boolean isExist(Integer value) {
        JudgeEnum judgeEnum = valueOf(value);
        return judgeEnum != null;
    }
}

package net.i.xiangadmin.module.system.role.basic.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.i.xiangadmin.common.domain.BaseEntity;
import lombok.Data;

/**
 * [ 角色 实体 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
@TableName("t_role")
public class RoleEntity extends BaseEntity {

    private String roleName;

    private String remark;
}

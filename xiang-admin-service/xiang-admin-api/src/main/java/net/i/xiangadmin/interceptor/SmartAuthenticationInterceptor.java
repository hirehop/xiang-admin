package net.i.xiangadmin.interceptor;

import com.alibaba.fastjson.JSONObject;
import net.i.xiangadmin.common.annotation.NoNeedLogin;
import net.i.xiangadmin.common.annotation.NoValidPrivilege;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.constant.CommonConst;
import net.i.xiangadmin.module.system.login.constant.LoginResponseCodeConst;
import net.i.xiangadmin.module.system.login.LoginTokenService;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import net.i.xiangadmin.module.system.privilege.service.PrivilegeEmployeeService;
import net.i.xiangadmin.util.SmartRequestTokenUtil;
import net.i.xiangadmin.util.SmartStringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * [ SpringBoot拦截器：登录拦截器]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Component
public class SmartAuthenticationInterceptor extends HandlerInterceptorAdapter {

    // 定义token名称
    public static final String TOKEN_NAME = "x-access-token";

    @Value("${access-control-allow-origin}")

    private String accessControlAllowOrigin;

    // 与登录相关的token服务
    @Autowired
    private LoginTokenService loginTokenService;

    // 员工权限服务
    @Autowired
    private PrivilegeEmployeeService privilegeEmployeeService;

    /**
     * 拦截服务器端响应处理ajax请求返回结果
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 跨域设置
        this.crossDomainConfig(response);
        boolean isHandlerMethod = handler instanceof HandlerMethod;
        if (! isHandlerMethod) {
            return true;
        }

        // 不需要登录的注解
        Boolean isNoNeedLogin = ((HandlerMethod) handler).getMethodAnnotation(NoNeedLogin.class) != null;
        if (isNoNeedLogin) {
            return true;
        }

        // 放行的Uri前缀
        String uri = request.getRequestURI();
        String contextPath = request.getContextPath();
        String target = uri.replaceFirst(contextPath, "");
        if (CommonConst.CommonCollection.contain(CommonConst.CommonCollection.IGNORE_URL, target)) {
            return true;
        }

        // 需要做token校验, 消息头的token优先于请求query参数的token
        // 获取到token   -> 拦截器放行
        // 获取不到token -> 将LOGIN_ERROR常量信息以json格式返回给浏览器/客户端
        String xHeaderToken = request.getHeader(TOKEN_NAME);
        String xRequestToken = request.getParameter(TOKEN_NAME);
        String xAccessToken = null != xHeaderToken ? xHeaderToken : xRequestToken;
        if (null == xAccessToken) {
            this.outputResult(response, LoginResponseCodeConst.LOGIN_ERROR);
            return false;
        }

        // 根据token获取登录用户
        // 获取到用户   ->   拦截器放行
        // 获取不到用户  ->  将LOGIN_ERROR常量信息以json格式返回给浏览器/客户端
        RequestTokenDTO requestToken = loginTokenService.getEmployeeTokenInfo(xAccessToken);
        if (null == requestToken) {
            this.outputResult(response, LoginResponseCodeConst.LOGIN_ERROR);
            return false;
        }

        /**
         * 判断接口权限 [拦截器执行到这里已经获取到正常的用户]
         */
        // 获得方法及方法名
        Method m = ((HandlerMethod) handler).getMethod();
        String methodName = ((HandlerMethod) handler).getMethod().getName();
        // 获取类Class及全类名
        Class<?> cls = ((HandlerMethod) handler).getBeanType();
        String className = ((HandlerMethod) handler).getBeanType().getName();
        // 将全类名拆分为List
        List<String> list = SmartStringUtil.splitConvertToList(className, "\\.");
        // 获取控制器名称
        String controllerName = list.get(list.size() - 1);

        // 判断方法和类上有无@NoValidPrivilege注解
        boolean isClzAnnotation = cls.isAnnotationPresent(NoValidPrivilege.class);
        boolean isMethodAnnotation = m.isAnnotationPresent(NoValidPrivilege.class);
        NoValidPrivilege noValidPrivilege = null;
        if (isClzAnnotation) {
            noValidPrivilege = cls.getAnnotation(NoValidPrivilege.class);
        } else if (isMethodAnnotation) {
            noValidPrivilege = m.getAnnotation(NoValidPrivilege.class);
        }
        // 不需验证权限
        if (noValidPrivilege != null) {
            SmartRequestTokenUtil.setUser(request, requestToken);
            return true;
        }
        // 需要验证权限
        // privilege/service/PrivilegeEmployeeService.java
        //     checkEmployeeHavePrivilege
        //         - token
        //         - 控制器名称（类名）
        //         - 方法名称
        Boolean privilegeValidPass = privilegeEmployeeService.checkEmployeeHavePrivilege(requestToken, controllerName, methodName);
        if (! privilegeValidPass) {
            this.outputResult(response, LoginResponseCodeConst.NOT_HAVE_PRIVILEGES);
            return false;
        }
        SmartRequestTokenUtil.setUser(request, requestToken);
        return true;
    }

    /**
     * 为HttpServletResponse配置跨域
     *
     * @param response
     */
    private void crossDomainConfig(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", accessControlAllowOrigin);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        response.setHeader("Access-Control-Expose-Headers", "*");
        response.setHeader("Access-Control-Allow-Headers", "Authentication,Origin, X-Requested-With, Content-Type, " + "Accept, x-access-token");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires ", "-1");
    }

    /**
     * 输出（拦截器拦截出错情况时调用）
     *
     * @param response
     * @param responseCodeConst
     * @throws IOException
     */
    private void outputResult(HttpServletResponse response, LoginResponseCodeConst responseCodeConst) throws IOException {
        ResponseDTO<Object> wrap = ResponseDTO.wrap(responseCodeConst);
        // 通过实体getter将属性以键值形式解析为json字符串
        String msg = JSONObject.toJSONString(wrap);
        // 设置响应数据格式及编码
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(msg);
        // 强制将数据返回客户端
        response.flushBuffer();
    }
}

package net.i.xiangadmin.module.system.login;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;
import net.i.xiangadmin.common.constant.JudgeEnum;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.constant.CommonConst;
import net.i.xiangadmin.module.system.log.LogService;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogEntity;
import net.i.xiangadmin.module.system.employee.EmployeeDao;
import net.i.xiangadmin.module.system.employee.constant.EmployeeResponseCodeConst;
import net.i.xiangadmin.module.system.employee.constant.EmployeeStatusEnum;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeLoginFormDTO;
import net.i.xiangadmin.module.system.login.domain.KaptchaVO;
import net.i.xiangadmin.module.system.login.domain.LoginDetailDTO;
import net.i.xiangadmin.module.system.login.domain.LoginPrivilegeDTO;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import net.i.xiangadmin.module.system.privilege.domain.entity.PrivilegeEntity;
import net.i.xiangadmin.module.system.privilege.service.PrivilegeEmployeeService;
import net.i.xiangadmin.util.XiangBeanUtil;
import net.i.xiangadmin.util.XiangDigestUtil;
import net.i.xiangadmin.util.SmartIPUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * [ 登录服务 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Slf4j
@Service
public class LoginService {

    private static final String VERIFICATION_CODE_REDIS_PREFIX = "vc_%s";

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private PrivilegeEmployeeService privilegeEmployeeService;

    @Autowired
    private LoginTokenService loginTokenService;

    @Autowired
    private LogService logService;

    @Autowired
    private DefaultKaptcha defaultKaptcha;

    /* REDIS缓存组件 */
    @Autowired
    private ValueOperations<String, String> redisValueOperations;

    /**
     * 登陆
     *
     * @param loginForm 登录名（loginName） 密码（loginPwd ）验证码uuid(codeUuid) 验证码（code）
     * @return 登录用户基本信息
     */
    public ResponseDTO<LoginDetailDTO> login(@Valid EmployeeLoginFormDTO loginForm, HttpServletRequest request) {
        // 验证码验证逻辑
        String redisVerificationCode = redisValueOperations.get(loginForm.getCodeUuid());
        redisValueOperations.getOperations().delete(loginForm.getCodeUuid());
        if (StringUtils.isEmpty(redisVerificationCode)) {
            return ResponseDTO.wrap(EmployeeResponseCodeConst.VERIFICATION_CODE_INVALID);
        }
        if (!redisVerificationCode.equalsIgnoreCase(loginForm.getCode())) {
            return ResponseDTO.wrap(EmployeeResponseCodeConst.VERIFICATION_CODE_INVALID);
        }
        // 密码验证逻辑
        String password = loginForm.getLoginPwd();
        String pwd = XiangDigestUtil.base64Decode(password);
        String loginPwd = XiangDigestUtil.encryptPassword(CommonConst.Password.SALT_FORMAT, pwd);
        EmployeeDTO employeeDTO = employeeDao.login(loginForm.getLoginName(), loginPwd);
        if (null == employeeDTO) {
            // 登录失败返回
            // 并不直接返回null，而是采用空对象设计模式处理返回值
            return ResponseDTO.wrap(EmployeeResponseCodeConst.LOGIN_FAILED);
        }
        // 用户被禁用
        if (EmployeeStatusEnum.DISABLED.equalsValue(employeeDTO.getIsDisabled())) {
            return ResponseDTO.wrap(EmployeeResponseCodeConst.IS_DISABLED);
        }

        // jwt token赋值（将token封装返回给vue）
        String compactJws = loginTokenService.generateToken(employeeDTO);

        // 对象拷贝（复制对象属性至新对象）
        // 登录成功后从数据库取出了EmployeeDTO, 登录模块传输的内容使用该模块内的DTO（约定）-> LoginDetailDTO
        LoginDetailDTO loginDTO = XiangBeanUtil.copy(employeeDTO, LoginDetailDTO.class);

        // 填充token
        loginDTO.setXAccessToken(compactJws);

        // 填充前端功能权限
        loginDTO.setPrivilegeList(initEmployeePrivilege(employeeDTO.getId()));

        // 判断是否为超管
        Boolean isSuperman = privilegeEmployeeService.isSuperman(loginDTO.getId());
        loginDTO.setIsSuperMan(isSuperman);

        // 记录登陆操作日志
        // eu.bitwalker.useragentutils类，用于快速解析浏览器User-Agent信息
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        UserLoginLogEntity logEntity =
                UserLoginLogEntity.builder()
                        .userId(employeeDTO.getId())
                        .userName(employeeDTO.getName())
                        .remoteIp(SmartIPUtil.getRemoteIp(request))
                        .remotePort(request.getRemotePort())
                        .remoteBrowser(userAgent.getBrowser().getName())
                        .remoteOs(userAgent.getOperatingSystem().getName())
                        .loginStatus(JudgeEnum.YES.getValue()).build();
        logService.addLog(logEntity);

        return ResponseDTO.succData(loginDTO);
    }

    /**
     * 退出登陆，清除token缓存
     *
     * @param requestToken
     * @return 退出登陆是否成功，bool
     */
    public ResponseDTO<Boolean> logoutByToken(RequestTokenDTO requestToken) {
        privilegeEmployeeService.removeCache(requestToken.getRequestUserId());
        return ResponseDTO.succ();
    }

    /**
     * 获取验证码（com.github.penggle:kaptcha:2.3.2开源组件）
     *
     * @return
     */
    public ResponseDTO<KaptchaVO> verificationCode() {
        KaptchaVO kaptchaVO = new KaptchaVO();
        String uuid = buildVerificationCodeRedisKey(UUID.randomUUID().toString());
        // 根据文本创建image
        String kaptchaText = defaultKaptcha.createText();
        BufferedImage image = defaultKaptcha.createImage(kaptchaText);
        String base64Code = "";
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", outputStream);
            base64Code = Base64.encodeBase64String(outputStream.toByteArray());
        } catch (Exception e) {
            log.error("verificationCode exception .{}", e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    log.error("verificationCode outputStream close exception .{}", e);
                }
            }
        }
        kaptchaVO.setUuid(uuid);
        kaptchaVO.setCode("data:image/png;base64," + base64Code);

        // 将[uuid=验证码文本]存入redis缓存中，60s过期时间
        redisValueOperations.set(uuid, kaptchaText, 60L, TimeUnit.SECONDS);

        return ResponseDTO.succData(kaptchaVO);
    }

    // 构建验证码Redis键
    private String buildVerificationCodeRedisKey(String uuid) {
        return String.format(VERIFICATION_CODE_REDIS_PREFIX, uuid);
    }

    /**
     * 初始化员工权限
     *
     * @param employeeId
     * @return
     */
    public List<LoginPrivilegeDTO> initEmployeePrivilege(Long employeeId) {
        // 根据员工ID获取该员工权限List
        List<PrivilegeEntity> privilegeList = privilegeEmployeeService.getPrivilegesByEmployeeId(employeeId);
        // 更新本地缓存
        privilegeEmployeeService.updateCachePrivilege(employeeId, privilegeList);
        // 登录模块中的权限DTO
        return XiangBeanUtil.copyList(privilegeList, LoginPrivilegeDTO.class);
    }

    /**
     * 获取Session
     *
     * @param requestUser
     * @return
     */
    public LoginDetailDTO getSession(RequestTokenDTO requestUser) {
        LoginDetailDTO loginDTO = XiangBeanUtil.copy(requestUser.getEmployeeBO(), LoginDetailDTO.class);
        // 从缓存中读取用户权限
        List<PrivilegeEntity> privilegeEntityList = privilegeEmployeeService.getEmployeeAllPrivilege(requestUser.getRequestUserId());
        // 缓存中不存在当前用户，读取数据库缓存当前用户权限
        if (privilegeEntityList == null) {
            List<LoginPrivilegeDTO> loginPrivilegeDTOS = initEmployeePrivilege(requestUser.getRequestUserId());
            loginDTO.setPrivilegeList(loginPrivilegeDTOS);
        } else {
            loginDTO.setPrivilegeList(XiangBeanUtil.copyList(privilegeEntityList, LoginPrivilegeDTO.class));
        }

        //判断是否为超管
        Boolean isSuperman = privilegeEmployeeService.isSuperman(loginDTO.getId());
        loginDTO.setIsSuperMan(isSuperman);
        return loginDTO;
    }
}

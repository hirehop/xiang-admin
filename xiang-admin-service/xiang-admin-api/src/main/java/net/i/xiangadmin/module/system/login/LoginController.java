package net.i.xiangadmin.module.system.login;

import net.i.xiangadmin.common.annotation.NoNeedLogin;
import net.i.xiangadmin.common.annotation.NoValidPrivilege;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.constant.SwaggerTagConst;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeLoginFormDTO;
import net.i.xiangadmin.module.system.login.constant.LoginResponseCodeConst;
import net.i.xiangadmin.module.system.login.domain.KaptchaVO;
import net.i.xiangadmin.module.system.login.domain.LoginDetailDTO;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import net.i.xiangadmin.util.SmartRequestTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * [ 后台登录、登出 ]
 * 返回结果统一使用ResponseDTO封装（code+message+success+data）
 */
@RestController
@Api(tags = {SwaggerTagConst.Admin.MANAGER_USER_LOGIN})
public class LoginController {

    @Autowired
    private LoginService loginService;

    @GetMapping("/session/verificationCode")
    @ApiOperation(value = "获取验证码", notes = "获取验证码")
    @NoNeedLogin
    public ResponseDTO<KaptchaVO> verificationCode() {
        return loginService.verificationCode();
    }

    /**
     * 将登录信息封装为DTO -> 员工登录表单（EmployeeLoginForm + DTO）
     * @param loginForm
     * @param request
     * @return
     */
    @PostMapping("/session/login")
    @ApiOperation(value = "登录", notes = "登录")
    @NoNeedLogin
    public ResponseDTO<LoginDetailDTO> login(@Valid @RequestBody EmployeeLoginFormDTO loginForm, HttpServletRequest request) {
        return loginService.login(loginForm, request);
    }

    @GetMapping("/session/get")
    @ApiOperation(value = "获取session", notes = "获取session")
    @NoValidPrivilege
    public ResponseDTO<LoginDetailDTO> getSession() {
        RequestTokenDTO requestUser = SmartRequestTokenUtil.getRequestUser();
        return ResponseDTO.succData(loginService.getSession(requestUser));
    }

    @GetMapping("/session/logOut")
    @ApiOperation(value = "退出登陆", notes = "退出登陆")
    @NoValidPrivilege
    public ResponseDTO<Boolean> logOut() {
        RequestTokenDTO requestToken = SmartRequestTokenUtil.getRequestUser();
        if (null == requestToken) {
            return ResponseDTO.wrap(LoginResponseCodeConst.LOGIN_ERROR);
        }
        // 移除当前用户的所有访问权限
        return loginService.logoutByToken(requestToken);
    }

}

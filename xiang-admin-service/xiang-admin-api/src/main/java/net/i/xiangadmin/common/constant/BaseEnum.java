package net.i.xiangadmin.common.constant;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONAware;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.CaseFormat;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Objects;

/**
 * [ 自定义Enum基类 -> BaseEnum = (value+desc)'s getter ]
 * [ 默认方法：boolean equalsValue(Object value) + boolean equals(BaseEnum baseEnum) ]
 * [ 静态方法：String getInfo(Class<? extends BaseEnum> clazz) ]
 *
 * @version 1.0
 * @since JDK1.8
 */
public interface BaseEnum {

    /**
     * 获取枚举类的值
     *
     * @return Object
     */
    Object getValue();

    /**
     * 获取枚举类的说明
     *
     * @return String
     */
    String getDesc();

    /**
     * 比较参数是否与枚举类的value相同
     *
     * @param value
     * @return boolean
     */
    default boolean equalsValue(Object value) {
        return Objects.equals(getValue(), value);
    }

    /**
     * 比较枚举类是否相同
     *
     * @param baseEnum
     * @return boolean
     */
    default boolean equals(BaseEnum baseEnum) {
        return Objects.equals(getValue(), baseEnum.getValue()) && Objects.equals(getDesc(), baseEnum.getDesc());
    }

    /**
     * 返回枚举类的说明
     *
     * @param clazz 枚举类类对象
     * @return
     */
    static String getInfo(Class<? extends BaseEnum> clazz) {
        BaseEnum[] enums = clazz.getEnumConstants();
        LinkedHashMap<String, JSONObject> json = new LinkedHashMap<>(enums.length);
        for (BaseEnum e : enums) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", new IJsonAware(e.getValue()));
            jsonObject.put("desc", new IJsonAware(e.getDesc()));
            json.put(e.toString(), jsonObject);
        }
        /* 转换\、\t、\n为html内容 */
        String enumJson = JSON.toJSONString(json, true);
        enumJson = enumJson.replaceAll("\"", "");
        enumJson = enumJson.replaceAll("\t", "&nbsp;&nbsp;");
        enumJson = enumJson.replaceAll("\n", "<br>");
        /* guava包，名称CamelCase转换为下划线全大写 */
        String prefix = "  <br> export const <br> " + CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, clazz.getSimpleName() + " = <br> ");
        return prefix + "" + enumJson + " <br>";
    }


    /**
     * [ JSONAware中声明了toJSONString方法 ]
     */
    @Data
    class IJsonAware implements JSONAware {

        private String value;

        public IJsonAware(Object value) {
            if (value == null) {
                this.value = "";
            } else if (value instanceof String) {
                this.value = "'" + value + "'";
            } else {
                this.value = value.toString();
            }
        }

        @Override
        public String toJSONString() {
            return value;
        }
    }
}

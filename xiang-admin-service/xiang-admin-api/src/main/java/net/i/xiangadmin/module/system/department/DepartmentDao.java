package net.i.xiangadmin.module.system.department;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.i.xiangadmin.module.system.department.domain.dto.DepartmentVO;
import net.i.xiangadmin.module.system.department.domain.entity.DepartmentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * t_department dao接口
 */
@Mapper
@Component
public interface DepartmentDao extends BaseMapper<DepartmentEntity> {

    /**
     * 根据部门id，查询此部门直接子部门的数量
     *
     * @param deptId
     * @return int 子部门的数量
     */
    Integer countSubDepartment(@Param("deptId") Long deptId);

    /**
     * 获取全部部门列表
     *
     * @return List<DepartmentVO>
     */
    List<DepartmentVO> listAll();

    /**
     * 功能描述: 根据父部门id查询
     *
     * @param
     * @return
     */
    List<DepartmentVO> selectByParentId(@Param("departmentId") Long departmentId);

}

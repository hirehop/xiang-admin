package net.i.xiangadmin.module.business.file.constant;

import net.i.xiangadmin.common.constant.BaseEnum;

/**
 * [ 文件服务枚举类，locationType与serviceName共同确定文件服务来源 ]
 * [ 现只支持Local ]
 */
public enum FileServiceTypeEnum implements BaseEnum {

    /**
     * 本地文件服务
     */
    LOCAL(1, FileServiceNameConst.LOCAL, "本地文件服务");

    private Integer locationType;

    private String serviceName;

    private String desc;

    FileServiceTypeEnum(Integer locationType, String serviceName, String desc) {
        this.locationType = locationType;
        this.serviceName = serviceName;
        this.desc = desc;
    }

    public String getServiceName() {
        return serviceName;
    }
    @Override
    public Integer getValue() {
        return this.locationType;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}

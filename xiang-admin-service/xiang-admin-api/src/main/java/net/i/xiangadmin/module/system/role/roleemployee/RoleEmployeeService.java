package net.i.xiangadmin.module.system.role.roleemployee;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.module.system.department.DepartmentDao;
import net.i.xiangadmin.module.system.department.domain.entity.DepartmentEntity;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO;
import net.i.xiangadmin.module.system.role.basic.RoleDao;
import net.i.xiangadmin.module.system.role.basic.RoleResponseCodeConst;
import net.i.xiangadmin.module.system.role.basic.domain.dto.RoleBatchDTO;
import net.i.xiangadmin.module.system.role.basic.domain.dto.RoleQueryDTO;
import net.i.xiangadmin.module.system.role.basic.domain.dto.RoleSelectedVO;
import net.i.xiangadmin.module.system.role.basic.domain.entity.RoleEntity;
import net.i.xiangadmin.module.system.role.roleemployee.domain.RoleEmployeeEntity;
import net.i.xiangadmin.util.XiangBeanUtil;
import net.i.xiangadmin.util.XiangPageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色管理业务
 *
 * @date 2019/4/3
 */
@Service
public class RoleEmployeeService {

    @Autowired
    private RoleEmployeeDao roleEmployeeDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private DepartmentDao departmentDao;

    /**
     * 通过角色id，分页获取成员员工列表
     *
     * @param queryDTO
     * @return
     */
    public ResponseDTO<PageResultDTO<EmployeeDTO>> listEmployeeByName(RoleQueryDTO queryDTO) {
        Page page = XiangPageUtil.convert2QueryPage(queryDTO);
        List<EmployeeDTO> employeeDTOS = roleEmployeeDao.selectEmployeeByNamePage(page, queryDTO);

        // 根据部门ID获取部门名称，填充进EmployeeDTO
        List<Long> ids = employeeDTOS.stream().map(EmployeeDTO::getDepartmentId).collect(Collectors.toList());
        if(ids.size() > 0) {
            List<DepartmentEntity> departmentEntities = departmentDao.selectBatchIds(ids);
            for (DepartmentEntity entity : departmentEntities) {
                Long id = entity.getId();
                for (EmployeeDTO employeeDTO : employeeDTOS) {
                    if (employeeDTO.getDepartmentId().equals(id)) {
                        employeeDTO.setDepartmentName(entity.getName());
                    }
                }
            }
        }

        PageResultDTO<EmployeeDTO> pageResultDTO = XiangPageUtil.convert2PageResult(page, employeeDTOS, EmployeeDTO.class);
        return ResponseDTO.succData(pageResultDTO);
    }

    public ResponseDTO<List<EmployeeDTO>> getAllEmployeeByRoleId(Long roleId) {
        List<net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO> employeeDTOS = roleEmployeeDao.selectEmployeeByRoleId(roleId);
        List<EmployeeDTO> list = XiangBeanUtil.copyList(employeeDTOS, EmployeeDTO.class);
        return ResponseDTO.succData(list);
    }

    /**
     * 移除员工角色
     *
     * @param employeeId
     * @param roleId
     * @return ResponseDTO<String>
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> removeEmployeeRole(Long employeeId, Long roleId) {
        if (null == employeeId || null == roleId) {
            return ResponseDTO.wrap(RoleResponseCodeConst.ERROR_PARAM);
        }
        roleEmployeeDao.deleteByEmployeeIdRoleId(employeeId, roleId);
        return ResponseDTO.succ();
    }

    /**
     * 批量删除角色的成员员工
     *
     * @param removeDTO
     * @return ResponseDTO<String>
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> batchRemoveEmployeeRole(RoleBatchDTO removeDTO) {
        List<Long> employeeIdList = removeDTO.getEmployeeIds();
        roleEmployeeDao.batchDeleteEmployeeRole(removeDTO.getRoleId(), employeeIdList);
        return ResponseDTO.succ();
    }

    /**
     * 批量添加角色的成员员工
     *
     * @param addDTO
     * @return ResponseDTO<String>
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> batchAddEmployeeRole(RoleBatchDTO addDTO) {
        Long roleId = addDTO.getRoleId();
        List<Long> employeeIdList = addDTO.getEmployeeIds();
        roleEmployeeDao.deleteByRoleId(roleId);
        List<RoleEmployeeEntity> roleRelationEntities = Lists.newArrayList();
        RoleEmployeeEntity employeeRoleRelationEntity;
        for (Long employeeId : employeeIdList) {
            employeeRoleRelationEntity = new RoleEmployeeEntity();
            employeeRoleRelationEntity.setRoleId(roleId);
            employeeRoleRelationEntity.setEmployeeId(employeeId);
            roleRelationEntities.add(employeeRoleRelationEntity);
        }
        roleEmployeeDao.batchInsert(roleRelationEntities);
        return ResponseDTO.succ();
    }

    /**
     * 通过员工id获取员工角色
     *
     * @param employeeId
     * @return
     */
    public ResponseDTO<List<RoleSelectedVO>> getRolesByEmployeeId(Long employeeId) {
        List<Long> roleIds = roleEmployeeDao.selectRoleIdByEmployeeId(employeeId);
        List<RoleEntity> roleList = roleDao.selectList(null);
        List<RoleSelectedVO> result = XiangBeanUtil.copyList(roleList, RoleSelectedVO.class);
        result.stream().forEach(item -> item.setSelected(roleIds.contains(item.getId())));
        return ResponseDTO.succData(result);
    }
}

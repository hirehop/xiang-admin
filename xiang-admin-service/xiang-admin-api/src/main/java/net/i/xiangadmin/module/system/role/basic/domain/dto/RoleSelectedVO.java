package net.i.xiangadmin.module.system.role.basic.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * [ 员工选择VO <- RoleVO ]
 *
 */
@Data
public class RoleSelectedVO extends RoleVO {

    @ApiModelProperty("选择标记")
    private Boolean selected;
}

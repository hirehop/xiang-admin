package net.i.xiangadmin.module.business.file;

import net.i.xiangadmin.common.annotation.NoNeedLogin;
import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.constant.SwaggerTagConst;
import net.i.xiangadmin.module.business.file.constant.FileServiceTypeEnum;
import net.i.xiangadmin.module.business.file.domain.dto.FileAddDTO;
import net.i.xiangadmin.module.business.file.domain.dto.FileQueryDTO;
import net.i.xiangadmin.module.business.file.domain.vo.FileVO;
import net.i.xiangadmin.module.business.file.domain.vo.UploadVO;
import net.i.xiangadmin.module.business.file.service.FileService;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import net.i.xiangadmin.util.SmartRequestTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * [ 文件服务控制器 ]
 */
@RestController
@Api(tags = {SwaggerTagConst.Admin.MANAGER_FILE})
public class FileController {

    @Autowired
    private FileService fileService;

    private static final String FILE_PATH = "file/teacher";

    @ApiOperation(value = "系统文件查询")
    @PostMapping("/api/file/query")
    public ResponseDTO<PageResultDTO<FileVO>> queryListByPage(@RequestBody FileQueryDTO queryDTO, HttpServletRequest request) {
        return fileService.queryListByPage(queryDTO, request);
    }

    @ApiOperation(value = "文件本地上传", notes = "文件本地上传")
    @PostMapping("/api/file/localUpload/{fileType}/{courseId}")
    public ResponseDTO<UploadVO> localUpload(MultipartFile file, @PathVariable Integer fileType, @PathVariable Long courseId) throws Exception {
        return fileService.fileUpload(file, FileServiceTypeEnum.LOCAL, FILE_PATH, fileType, courseId);
    }

    @ApiOperation(value = "本地文件下载")
    @GetMapping("/api/file/downLoad")
    @NoNeedLogin
    public ResponseEntity<byte[]> downLoadById(Long id, HttpServletRequest request) {
        return fileService.downLoadById(id, request);
    }

    @ApiOperation(value = "系统文件保存（存储进数据库）")
    @PostMapping("/api/file/save")
    public ResponseDTO<String> saveFile(@Valid @RequestBody FileAddDTO addDTO) {
        RequestTokenDTO requestToken = SmartRequestTokenUtil.getRequestUser();
        return fileService.saveFile(addDTO,requestToken);
    }

    @ApiOperation(value = "本地文件删除")
    @GetMapping("/api/file/delete")
    @NoNeedLogin
    public ResponseDTO<String> deleteById(Long id, HttpServletRequest request) {
        return fileService.deleteById(id, request);
    }
}

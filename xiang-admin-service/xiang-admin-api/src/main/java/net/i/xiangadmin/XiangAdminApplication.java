package net.i.xiangadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * [ admin 项目启动类 ]
 *
 * @since JDK1.8
 *
 */
@SpringBootApplication(scanBasePackages = {"net.i.xiangadmin", "cn.afterturn.easypoi"})
@EnableCaching
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
public class XiangAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiangAdminApplication.class, args);
    }
}

package net.i.xiangadmin.module.system.role.roleprivilege.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.i.xiangadmin.common.annotation.ApiModelPropertyEnum;
import net.i.xiangadmin.module.system.privilege.constant.PrivilegeTypeEnum;

import java.util.List;

/**
 * 角色功能权限
 *
 */
@Data
public class RolePrivilegeSimpleDTO {

    @ApiModelProperty("父级Key")
    private String parentKey;

    /* 权限名称 */
    @ApiModelProperty("名称")
    private String name;

    /* 权限类型（菜单+功能点） */
    @ApiModelPropertyEnum(enumDesc = "类型",value = PrivilegeTypeEnum.class)
    private Integer type;

    /* 权限Key */
    @ApiModelProperty("key")
    private String key;

    /* 权限URL */
    @ApiModelProperty("url")
    private String url;

    @ApiModelProperty("排序")
    private Integer sort;

    /* 子级权限List<自身> */
    @ApiModelProperty("子级列表")
    private List<RolePrivilegeSimpleDTO> children;
}

package net.i.xiangadmin.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.i.xiangadmin.common.domain.page.OrderItemDTO;
import net.i.xiangadmin.common.domain.page.PageParamDTO;
import net.i.xiangadmin.common.domain.page.PageResultDTO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * [ 分页工具类 ]
 */

public class XiangPageUtil {

    /**
     * [ 将PageParamDTO转换为MP原生Page，即获取原生Page对象完成分页操作 ]
     * @param baseDTO PageParamDTO 包含当前页码pageNum、页面大小pageSize、是否查询总条数searchCount、排序List<OrderItemDTO> orders
     * @param <T>
     * @return
     */
    public static <T> Page<T> convert2QueryPage(PageParamDTO baseDTO) {
        Page<T> page = new Page<>();

        page.setCurrent(baseDTO.getPageNum());
        page.setSize(baseDTO.getPageSize());
        if (null != baseDTO.getSearchCount()) {
            page.setSearchCount(baseDTO.getSearchCount());
        }
        List<OrderItemDTO> orders = baseDTO.getOrders();
        if (orders != null && !orders.isEmpty()) {
            List<com.baomidou.mybatisplus.core.metadata.OrderItem> orderItemList = orders.stream().map(XiangPageUtil::convertOrderItem).collect(Collectors.toList());
            page.setOrders(orderItemList);
        }
        return page;
    }

    /**
     * 将自定义OrderItemDTO转换为MyBatisPlus中原生OrderItem（读取属性进行初始化）
     * @param orderItemDTO
     * @return
     */
    private static com.baomidou.mybatisplus.core.metadata.OrderItem convertOrderItem(OrderItemDTO orderItemDTO) {
        if (orderItemDTO.isAsc()) {
            return com.baomidou.mybatisplus.core.metadata.OrderItem.asc(orderItemDTO.getColumn());
        } else {
            return com.baomidou.mybatisplus.core.metadata.OrderItem.desc(orderItemDTO.getColumn());
        }
    }

    public static <T> PageResultDTO<T> convert2PageResult(IPage<T> page) {
        PageResultDTO<T> result = new PageResultDTO<>();
        result.setPageNum(page.getCurrent());
        result.setPageSize(page.getSize());
        result.setTotal(page.getTotal());
        result.setPages(page.getPages());
        result.setList(page.getRecords());
        return result;
    }

    /**
     * 转换为 PageResultDTO 对象
     *
     * @param page
     * @param sourceList  原list
     * @param targetClazz 目标类
     * @return
     */
    public static <T, E> PageResultDTO<T> convert2PageResult(IPage page, List<E> sourceList, Class<T> targetClazz) {
        PageResultDTO pageResultDTO = setPage(page);
        List<T> records = XiangBeanUtil.copyList(sourceList, targetClazz);
        page.setRecords(records);
        pageResultDTO.setList(records);
        return pageResultDTO;
    }

    /**
     * 转换为 PageResultDTO 对象
     *
     * @param page
     * @param sourceList list
     * @return
     */
    public static <T, E> PageResultDTO<T> convert2PageResult(IPage page, List<E> sourceList) {
        PageResultDTO pageResultDTO = setPage(page);
        page.setRecords(sourceList);
        pageResultDTO.setList(sourceList);
        return pageResultDTO;
    }

    private static PageResultDTO setPage(IPage page) {
        PageResultDTO pageResultDTO = new PageResultDTO();
        pageResultDTO.setPageNum(page.getCurrent());
        pageResultDTO.setPageSize(page.getSize());
        pageResultDTO.setTotal(page.getTotal());
        pageResultDTO.setPages(page.getPages());
        return pageResultDTO;
    }
}

package net.i.xiangadmin.common.domain;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
public interface ITask {

    void execute(String paramJson) throws Exception;
}

package net.i.xiangadmin.module.system.login.domain;

import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO;
import lombok.Data;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class LoginCacheDTO {

    /**
     * 基本信息
     */
    private EmployeeDTO employeeDTO;

    /**
     * 过期时间
     */
    private Long expireTime;
}

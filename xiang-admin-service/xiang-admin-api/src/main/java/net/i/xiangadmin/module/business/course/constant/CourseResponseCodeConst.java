package net.i.xiangadmin.module.business.course.constant;

import net.i.xiangadmin.common.constant.ResponseCodeConst;

public class CourseResponseCodeConst extends ResponseCodeConst {

    public static final CourseResponseCodeConst COURSE_REPEAT = new CourseResponseCodeConst(8001, "课程重复！");

    public CourseResponseCodeConst(int code, String msg) {
        super(code, msg);
    }
}

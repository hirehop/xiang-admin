package net.i.xiangadmin.module.business.file.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * [ 文件新增DTO：文件名+路径 ]
 */
@Data
public class FileAddDTO {

    @ApiModelProperty("文件名称")
    @NotBlank(message = "文件名称不能为空")
    private String fileName;

    @ApiModelProperty("文件路径")
    @NotBlank(message = "文件路径不能为空")
    private String filePath;

    @ApiModelProperty("文件大小")
    private String fileSize;

    @ApiModelProperty("文件类型")
    private Integer fileType;

    @ApiModelProperty("所属课程ID")
    private Long courseId;
}

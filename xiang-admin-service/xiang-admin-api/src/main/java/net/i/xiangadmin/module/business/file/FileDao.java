package net.i.xiangadmin.module.business.file;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.i.xiangadmin.module.business.file.domain.dto.FileDTO;
import net.i.xiangadmin.module.business.file.domain.dto.FileQueryDTO;
import net.i.xiangadmin.module.business.file.domain.entity.FileEntity;
import net.i.xiangadmin.module.business.file.domain.vo.FileVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * [ 文件DAO ]
 */
@Mapper
@Component
public interface FileDao extends BaseMapper<FileEntity> {

    /**
     * 批量添加上传文件
     *
     * @param fileDTOList
     * @return
     */
    Integer insertFileBatch(List<FileDTO> fileDTOList);


    /**
     * 批量添加上传文件
     *
     * @param fileDTOList
     * @return
     */
    Integer insertFileEntityBatch(List<FileEntity> fileDTOList);

    /**
     * 根据文件类型删除（将会批量删除数据）
     *
     * @param fileType
     * @return
     */
    Integer deleteFilesByFileType(@Param("fileType") Integer fileType);
    List<FileVO> listFilesByFileType(@Param("fileType") Integer fileType);

    List<FileVO> listFilesByFileIds(@Param("fileIds") List<Long> fileIds);

    List<FileVO> queryListByPage(Page page, @Param("queryDTO") FileQueryDTO queryDTO);
}

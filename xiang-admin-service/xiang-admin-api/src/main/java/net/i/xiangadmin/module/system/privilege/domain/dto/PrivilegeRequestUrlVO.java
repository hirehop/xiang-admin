package net.i.xiangadmin.module.system.privilege.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class PrivilegeRequestUrlVO {

    @ApiModelProperty("url")
    private String url;

    @ApiModelProperty("controller.method")
    private String name;

    @ApiModelProperty("注释说明")
    private String comment;
}

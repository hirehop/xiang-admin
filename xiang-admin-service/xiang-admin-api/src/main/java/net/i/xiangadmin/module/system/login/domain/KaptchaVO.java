package net.i.xiangadmin.module.system.login.domain;

import lombok.Data;

/**
 * [ 验证码存储实体类，uuid+base64CODE ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class KaptchaVO {

    /**
     *  验证码UUID
     */
    private String uuid;

    /**
     * base64 验证码（编码后的字符串允许直接嵌入img标签的src属性中）
     */
    private String code;

}

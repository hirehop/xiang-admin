package net.i.xiangadmin.module.system.login.constant;

import net.i.xiangadmin.common.constant.ResponseCodeConst;

/**
 * 登录常量类（1001~1999）
 */
public class LoginResponseCodeConst extends ResponseCodeConst {

    public static final LoginResponseCodeConst LOGIN_ERROR = new LoginResponseCodeConst(1001, "您还未登录或登录失效，请重新登录！");

    public static final LoginResponseCodeConst NOT_HAVE_PRIVILEGES = new LoginResponseCodeConst(1002, "对不起，您没有权限！");

    public LoginResponseCodeConst(int code, String msg) {
        super(code, msg);
    }
}

package net.i.xiangadmin.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Base64;

/**
 * 摘要工具类
 * 1.md5加密
 * 2.base64解密（与前端适配）
 */
public class XiangDigestUtil extends DigestUtils {

    /**
     * md5加盐加密
     *
     * @param salt
     * @param password
     * @return
     */
    public static String encryptPassword(String salt, String password) {
        return XiangDigestUtil.md5Hex(String.format(salt, password));
    }

    /**
     * [ 自定义base64解密操作，与前端硬性匹配：倒置后去除首位 ]
     *
     * @param str
     * @return
     */
    public static String base64Decode(String str) {
        str = new StringBuffer(str).reverse().toString().substring(1);
        byte[] decoded = Base64.getDecoder().decode(str);
        return new String(decoded);
    }
}

package net.i.xiangadmin.module.system.employee.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.i.xiangadmin.util.XiangVerificationUtil;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 添加员工
 */
@Data
public class EmployeeAddDTO {

    @ApiModelProperty("姓名")
    @NotNull(message = "姓名不能为空")
    private String name;

    @ApiModelProperty("登录名")
    @NotNull(message = "姓名不能为空")
    private String loginName;

    @ApiModelProperty("部门id")
    @NotNull(message = "部门id不能为空")
    private Long departmentId;

    @ApiModelProperty("是否启用")
    @NotNull(message = "是否被禁用不能为空")
    private Integer isDisabled;

    @ApiModelProperty("手机号")
    @NotNull(message = "手机号不能为空")
    @Pattern(regexp = XiangVerificationUtil.PHONE_REGEXP, message = "手机号格式不正确")
    private String phone;

    @ApiModelProperty("生日")
    private String birthday;

    @ApiModelProperty("密码")
    @NotNull(message = "密码不能为空")
    @Length(min = 6, message = "密码最少为6位字符")
    private String loginPwd;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("性别")
    private String gender;

    @ApiModelProperty("学历")
    private String degree;
}

package net.i.xiangadmin.listener;

import net.i.xiangadmin.common.constant.ResponseCodeConst;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * [ 应用启动监听器，启动后检测code码（加载响应码常量类） ]
 *
 * @version 1.0
 * @since JDK1.8
 */

@Component
public class SmartAdminStartupRunner implements CommandLineRunner {

    @Override
    public void run(String... args) {
        ResponseCodeConst.init();
    }
}
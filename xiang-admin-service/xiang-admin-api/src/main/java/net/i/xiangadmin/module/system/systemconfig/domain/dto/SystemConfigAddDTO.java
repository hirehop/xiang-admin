package net.i.xiangadmin.module.system.systemconfig.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * [ add DTO，在当前类中对前端传入的数据内容进行基本格式校验 ]
 * [ 参数键、参数值、参数名称、参数类别、参数备注（移除了entity中的`Base`和isUsing四个属性） ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class SystemConfigAddDTO {

    @ApiModelProperty("参数key")
    @NotBlank(message = "参数key不能为空")
    @Length(max = 255, message = "参数key最多255个字符")
    private String configKey;

    @ApiModelProperty("参数的值")
    @NotBlank(message = "参数的值不能为空")
    @Length(max = 65530, message = "参数的值最多65530个字符")
    private String configValue;

    @ApiModelProperty("参数名称")
    @NotBlank(message = "参数名称不能为空")
    @Length(max = 255, message = "参数名称最多255个字符")
    private String configName;

    @ApiModelProperty("参数类别")
    @NotBlank(message = "参数类别不能为空")
    @Length(max = 255, message = "参数类别最多255个字符")
    private String configGroup;

    @ApiModelProperty("备注")
    @Length(max = 255, message = "备注最多255个字符")
    private String remark;
}

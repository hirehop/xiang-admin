package net.i.xiangadmin.module.system.employee.domain.dto;

import net.i.xiangadmin.common.domain.page.PageParamDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 员工列表DTO
 */
@Data
public class EmployeeQueryDTO extends PageParamDTO {

    private String phone;

    private String name;

    private Long departmentId;

    private Integer isLeave;

    private Integer isDisabled;

    /**
     * 删除状态 0否 1是
     */
    @ApiModelProperty("删除状态 0否 1是 不需要传")
    private Integer isDelete;

    @ApiModelProperty("员工id集合")
    private List<Long> employeeIds;

}

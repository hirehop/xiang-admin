package net.i.xiangadmin.module.business.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.module.business.course.constant.CourseResponseCodeConst;
import net.i.xiangadmin.module.business.course.constant.ExamTimePartEnum;
import net.i.xiangadmin.module.business.course.dao.TCourseDao;
import net.i.xiangadmin.module.business.course.domain.dto.TCourseAddDTO;
import net.i.xiangadmin.module.business.course.domain.dto.TCourseQueryDTO;
import net.i.xiangadmin.module.business.course.domain.dto.TCourseUpdateDTO;
import net.i.xiangadmin.module.business.course.domain.entity.TCourseEntity;
import net.i.xiangadmin.module.business.course.domain.vo.TCourseVO;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import net.i.xiangadmin.util.SmartBaseEnumUtil;
import net.i.xiangadmin.util.SmartRequestTokenUtil;
import net.i.xiangadmin.util.XiangBeanUtil;
import net.i.xiangadmin.util.XiangPageUtil;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [ ${tableDesc} ]
 *
 * @author X
 * @version 1.0
 * @since JDK1.8
 */
@Service
public class TCourseService {

    @Autowired
    private TCourseDao tCourseDao;

    private static Map<Long, boolean[][]> courseRepeatCheckMap = new HashMap<>();


    /**
     * 根据id查询
     */
    public TCourseEntity getById(Long id) {
        return tCourseDao.selectById(id);
    }

    /**
     * 分页查询
     *
     * @author X
     * @date 2021-04-18 14:12:50
     */
    public ResponseDTO<PageResultDTO<TCourseVO>> queryByPage(TCourseQueryDTO queryDTO, HttpServletRequest request) {
        Long requestUserId = ((RequestTokenDTO) (request.getAttribute(SmartRequestTokenUtil.USER_KEY))).getRequestUserId();
        if (!requestUserId.equals(1L)) {
            queryDTO.setEmployeeId(requestUserId);
        }

        Page page = XiangPageUtil.convert2QueryPage(queryDTO);
        IPage<TCourseVO> voList = tCourseDao.queryByPage(page, queryDTO);
        /*
            参数一传Page，结果使用IPage接收，MP将自动完成分页查询操作；
            下方代码描述从IPage中取值的过程：
                PageResultDTO<T> result = new PageResultDTO<>();
                result.setPageNum(page.getCurrent());
                result.setPageSize(page.getSize());
                result.setTotal(page.getTotal());
                result.setPages(page.getPages());
                result.setList(page.getRecords());
                return result;
        */
        PageResultDTO<TCourseVO> pageResultDTO = XiangPageUtil.convert2PageResult(voList);

        // 根据上课周数再进行一次过滤
        List<TCourseVO> filterCourseList = Lists.newArrayList();
        Integer currentWeek = queryDTO.getWorkWeek();
        if(currentWeek!=null) {
            for (TCourseVO vo : pageResultDTO.getList()) {
                // 解析examTimePart为examTimePartDesc
                if(StringUtils.isNotEmpty(vo.getExamTimePart())) {
                    int examTimePart = Integer.parseInt(vo.getExamTimePart());
                    String examTimePartDesc = SmartBaseEnumUtil.getEnumDescByValue(examTimePart, ExamTimePartEnum.class);
                    vo.setExamTimePartDesc(examTimePartDesc);
                }
                String[] workWeeks = vo.getWorkWeeks().split("-");
                if (Integer.parseInt(workWeeks[0]) < currentWeek && currentWeek < Integer.parseInt(workWeeks[1])) {
                    filterCourseList.add(vo);
                }
            }
            pageResultDTO.setList(filterCourseList);
            pageResultDTO.setTotal((long) filterCourseList.size());
        }else{
            for (TCourseVO vo : pageResultDTO.getList()) {
                // 解析examTimePart为examTimePartDesc
                if(StringUtils.isNotEmpty(vo.getExamTimePart())) {
                    int examTimePart = Integer.parseInt(vo.getExamTimePart());
                    String examTimePartDesc = SmartBaseEnumUtil.getEnumDescByValue(examTimePart, ExamTimePartEnum.class);
                    vo.setExamTimePartDesc(examTimePartDesc);
                    filterCourseList.add(vo);
                }
            }
            pageResultDTO.setList(filterCourseList);
            pageResultDTO.setTotal((long) filterCourseList.size());
        }

        // 管理员查询时，会获取所有数据，此时，解析所有数据完成初始化
        if(requestUserId.equals(1L)){
            courseRepeatCheckMap.clear();
            List<TCourseVO> list = pageResultDTO.getList();
            for(TCourseVO vo : list){
                boolean[][] value = new boolean[5][7];
                String[] workWeekdayArr = vo.getWorkWeekday().split(";");
                String[] workTimePartArr = vo.getWorkTimePart().split(";");
                for(int i=0;i<workWeekdayArr.length;i++){
                    int workWeekday = Integer.parseInt(workWeekdayArr[i]);
                    String[] oneDayWorkTimePart = workTimePartArr[i].split(",");
                    for(String timePart : oneDayWorkTimePart){
                        int iTimePart = Integer.parseInt(timePart);
                        value[iTimePart-1][workWeekday-1] = true;
                    }
                }
                courseRepeatCheckMap.put(vo.getEmployeeId(),value);
            }
        }

        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * 添加
     * 添加时，应该首先对课程进行重复检测，如果课程有重复则不允许添加
     *
     * @author X
     * @date 2021-04-18 14:12:50
     */
    public ResponseDTO<String> add(TCourseAddDTO addDTO) {

        // 获取新增用户的所有课程信息（检验是否重复）
        Long employeeId = addDTO.getEmployeeId();
        boolean[][] value = courseRepeatCheckMap.get(employeeId);
        if(value == null){
            courseRepeatCheckMap.put(employeeId, new boolean[5][7]);
            value = courseRepeatCheckMap.get(employeeId);
        }
        String[] workWeekdayArr = addDTO.getWorkWeekday().split(";");
        String[] workTimePartArr = addDTO.getWorkTimePart().split(";");
        for(int i=0;i<workWeekdayArr.length;i++){
            int workWeekday = Integer.parseInt(workWeekdayArr[i]);
            String[] oneDayWorkTimePart = workTimePartArr[i].split(",");
            for(String timePart : oneDayWorkTimePart){
                int iTimePart = Integer.parseInt(timePart);
                if(value[iTimePart-1][workWeekday-1]){
                    return ResponseDTO.wrap(CourseResponseCodeConst.COURSE_REPEAT);
                }
            }
        }

        TCourseEntity entity = XiangBeanUtil.copy(addDTO, TCourseEntity.class);
        tCourseDao.insert(entity);
        return ResponseDTO.succ();
    }

    /**
     * 根据ID删除课程
     *
     * @param id
     * @param request
     * @return
     */
    public ResponseDTO<String> deleteById(Long id, HttpServletRequest request) {
        tCourseDao.deleteById(id);
        return ResponseDTO.succ();
    }


    /**
     * 编辑
     *
     * @author X
     * @date 2021-04-18 14:12:50
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> update(TCourseUpdateDTO updateDTO) {
        TCourseEntity entity = XiangBeanUtil.copy(updateDTO, TCourseEntity.class);
        tCourseDao.updateById(entity);
        return ResponseDTO.succ();
    }

    /**
     * 删除
     *
     * @author X
     * @date 2021-04-18 14:12:50
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> deleteByIds(List<Long> idList) {
        tCourseDao.deleteByIdList(idList);
        return ResponseDTO.succ();
    }
}

package net.i.xiangadmin.module.system.systemconfig;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.i.xiangadmin.module.system.systemconfig.domain.dto.SystemConfigQueryDTO;
import net.i.xiangadmin.module.system.systemconfig.domain.entity.SystemConfigEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统参数配置 t_system_config Dao层
 */
@Component
@Mapper
public interface SystemConfigDao extends BaseMapper<SystemConfigEntity> {

    List<SystemConfigEntity> selectAll();

    /**
     * 查询所有系统配置（分页）
     *
     * @param page
     * @return
     */
    List<SystemConfigEntity> selectSystemSettingList(Page page, @Param("queryDTO") SystemConfigQueryDTO queryDTO);


    SystemConfigEntity getByKey(@Param("key") String key);

    SystemConfigEntity getByKeyExcludeId(@Param("key") String key, @Param("excludeId") Long excludeId);

    List<SystemConfigEntity> getListByGroup(String group);

    SystemConfigEntity selectByKeyAndGroup(@Param("configKey") String configKey, @Param("group") String group);
}

package net.i.xiangadmin.module.business.file.constant;
/**
 * [ 文件服务名称常量 ]
 */
public class FileServiceNameConst {

    /**
     * [ 本地文件服务 ]
     */
    public static final String LOCAL = "local";
}

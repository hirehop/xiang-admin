package net.i.xiangadmin.module.system.log.userloginlog;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogQueryDTO;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * [ 用户登录日志 ]
 * [ 注意事项:（使用MP时，参数使用@Param注解，可以将参数名映射为value参数值后在mapper.xml配置文件中使用） ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Mapper
@Component
public interface UserLoginLogDao extends BaseMapper<UserLoginLogEntity> {

    /**
     * 分页查询
     * @param queryDTO (userName:String + startTime:String + endTime:String)
     * @return UserLoginLogEntity实体
    */
    List<UserLoginLogEntity> queryByPage(Page page, @Param("queryDTO") UserLoginLogQueryDTO queryDTO);

    /**
     * 根据id删除
     * @param id 用户登录日志ID
     * @return
    */
    void deleteById(@Param("id") Long id);

    /**
     * 批量删除
     * @param idList 用户登录日志ID列表
     * @return
    */
    void deleteByIds(@Param("idList") List<Long> idList);
}

package net.i.xiangadmin.module.system.role.basic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.i.xiangadmin.module.system.role.basic.domain.entity.RoleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Mapper
@Component
public interface RoleDao extends BaseMapper<RoleEntity> {


    RoleEntity getByRoleName(@Param("roleName") String name);

}

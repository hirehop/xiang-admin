package net.i.xiangadmin.module.system.systemconfig.domain.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.i.xiangadmin.common.domain.page.PageParamDTO;

/**
 * [ 系统参数查询DTO，分页+查询类别（key、configGroup） ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class SystemConfigQueryDTO extends PageParamDTO {

    @ApiModelProperty("参数KEY")
    private String key;

    @ApiModelProperty("参数类别")
    private String configGroup;

}

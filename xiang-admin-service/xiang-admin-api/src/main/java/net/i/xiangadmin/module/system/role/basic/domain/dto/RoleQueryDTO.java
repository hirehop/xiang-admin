package net.i.xiangadmin.module.system.role.basic.domain.dto;
import net.i.xiangadmin.common.domain.page.PageParamDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * [ DTO：角色查询专用（角色名称 + 角色ID） ，集成分页参数DTO]
 * 
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class RoleQueryDTO extends PageParamDTO {

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色id")
    private String roleId;
}

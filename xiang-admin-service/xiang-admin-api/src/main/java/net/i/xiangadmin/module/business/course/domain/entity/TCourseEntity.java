package net.i.xiangadmin.module.business.course.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import net.i.xiangadmin.common.domain.BaseEntity;

/**
 * [  ]
 *
 * @author X
 * @version 1.0
 * @since JDK1.8
 */
@Data
@TableName("t_course")
public class TCourseEntity extends BaseEntity {

    /**
     * 课程名
     */
    private String name;

    /**
     * 评教分数
     */
    private Double score;

    /**
     * 上课地点
     */
    private String workPlace;

    /**
     * 员工ID
     */
    private Long employeeId;

    /**
     * 开课日期
     */
    private String startDate;

    /**
     * 结课日期
     */
    private String endDate;

    /**
     * 工作日（星期N），可以多个，使用半角分号分隔
     */
    private String workWeekday;

    /**
     * 工作时间段，可以多个，一天内使用半角逗号分隔，不同天使用半角分号分隔
     */
    private String workTimePart;

    /**
     * 考试日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private String examDate;

    /**
     * 考试时间段
     */
    private String examTimePart;

    /**
     * 考试说明
     */
    private String examRemark;

    /**
     * 开课学年(2020-2021)
     */
    private String workYear;

    /**
     * 开课学期
     */
    private Integer workPart;

    /**
     * 开课周-结课周
     */
    private String workWeeks;
}
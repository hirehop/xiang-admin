package net.i.xiangadmin.module.business.file.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.i.xiangadmin.common.domain.BaseEntity;
import lombok.Data;


/**
 * 
 * [ 文件实体 ]
 * 
 * @version 1.0
 * @since JDK1.8
 */
@Data
@TableName(value = "t_file")
public class FileEntity extends BaseEntity {
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件大小
     */
    private String fileSize;
    /**
     * 文件类型，FileTypeEnum控制
     */
    private String fileType;
    /**
     * 文件路径（key），用于文件下载
     */
    private String filePath;
    /**
     * 创建人，即上传人
     */
    private Long createUser;

    /**
     * 当前文件所属课程ID
     */
    private Long courseId;
}


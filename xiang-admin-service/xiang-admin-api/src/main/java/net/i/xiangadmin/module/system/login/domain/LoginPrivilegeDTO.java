package net.i.xiangadmin.module.system.login.domain;

import net.i.xiangadmin.common.annotation.ApiModelPropertyEnum;
import net.i.xiangadmin.module.system.privilege.constant.PrivilegeTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * [ 菜单类型 + 权限key + URL + 父级Key ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class LoginPrivilegeDTO {

    @ApiModelPropertyEnum(enumDesc = "菜单类型",value = PrivilegeTypeEnum.class)
    private Integer type;

    @ApiModelProperty("权限key")
    private String key;

    @ApiModelProperty("url")
    private String url;

    @ApiModelProperty("父级key")
    private String parentKey;

}

package net.i.xiangadmin.module.system.log.userloginlog.domain;

import net.i.xiangadmin.common.domain.page.PageParamDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * [ 用户登录日志 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class UserLoginLogQueryDTO extends PageParamDTO {

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("开始日期")
    private String startDate;

    @ApiModelProperty("结束日期")
    private String endDate;
}

package net.i.xiangadmin.module.system.role.basic.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * [ 角色值对象VO = 角色ID+角色名称+角色备注 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class RoleVO {

    @ApiModelProperty("角色ID")
    private Long id;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色备注")
    private String remark;
}

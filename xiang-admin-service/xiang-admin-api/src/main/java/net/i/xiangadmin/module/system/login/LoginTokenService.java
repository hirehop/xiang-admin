package net.i.xiangadmin.module.system.login;

import net.i.xiangadmin.common.constant.JudgeEnum;
import net.i.xiangadmin.module.system.employee.EmployeeService;
import net.i.xiangadmin.module.system.employee.constant.EmployeeStatusEnum;
import net.i.xiangadmin.module.system.employee.domain.bo.EmployeeBO;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

/**
 * [ 生成token + 根据token获取登录信息（返回 RequestTokenBO业务对象 = requestUserId请求用户id + EmployeeBO员工业务对象） ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Slf4j
@Service
public class LoginTokenService {

    /**
     * 过期时间一天
     */
    private static final int EXPIRE_SECONDS = 1 * 24 * 3600;
    /**
     * jwt加密字段（Json web token加密字段）
     */
    private static final String CLAIM_ID_KEY = "id";

    @Value("${jwt.key}")
    private String jwtKey;

    @Autowired
    private EmployeeService employeeService;


    /**
     * 功能描述: 生成JWT TOKEN
     *
     * @param employeeDTO
     * @return
     */
    public String generateToken(EmployeeDTO employeeDTO) {
        Long id = employeeDTO.getId();
        /**将token设置为jwt格式*/
        String baseToken = UUID.randomUUID().toString();
        LocalDateTime localDateTimeNow = LocalDateTime.now();
        LocalDateTime localDateTimeExpire = localDateTimeNow.plusSeconds(EXPIRE_SECONDS);
        Date from = Date.from(localDateTimeNow.atZone(ZoneId.systemDefault()).toInstant());
        Date expire = Date.from(localDateTimeExpire.atZone(ZoneId.systemDefault()).toInstant());

        // io.jsonwebtoken依赖
        Claims jwtClaims = Jwts.claims().setSubject(baseToken);
        jwtClaims.put(CLAIM_ID_KEY, id);
        // 生成token
        String compactJws = Jwts.builder().setClaims(jwtClaims).setNotBefore(from).setExpiration(expire).signWith(SignatureAlgorithm.HS512, jwtKey).compact();

        return compactJws;
    }

    /**
     * 功能描述: 根据登陆token获取登陆信息
     *
     * @param
     * @return
     */
    public RequestTokenDTO getEmployeeTokenInfo(String token) {
        Long employeeId = -1L;
        try {
            // 获取token中的员工id
            Claims claims = Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(token).getBody();
            String idStr = claims.get(CLAIM_ID_KEY).toString();
            employeeId = Long.valueOf(idStr);
        } catch (Exception e) {
            log.error("getEmployeeTokenInfo error:{}", e);
            return null;
        }

        // 查询数据库，是否存在当前用户
        EmployeeBO employeeBO = employeeService.getById(employeeId);
        if (employeeBO == null) {
            return null;
        }

        // 是否被禁用
        if (EmployeeStatusEnum.DISABLED.getValue().equals(employeeBO.getIsDisabled())) {
            return null;
        }

        // 是否离职
        if (JudgeEnum.YES.equals(employeeBO.getIsLeave())) {
            return null;
        }

        // 是否被删除
        if (JudgeEnum.YES.equals(employeeBO.getIsDelete())) {
            return null;
        }

        return new RequestTokenDTO(employeeBO);
    }

}

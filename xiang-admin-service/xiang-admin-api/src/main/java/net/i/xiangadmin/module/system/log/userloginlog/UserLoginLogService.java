package net.i.xiangadmin.module.system.log.userloginlog;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.module.system.employee.EmployeeService;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeQueryDTO;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogDTO;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogEntity;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogQueryDTO;
import net.i.xiangadmin.util.XiangBeanUtil;
import net.i.xiangadmin.util.XiangPageUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * [ 用户登录日志 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Service
public class UserLoginLogService {

    @Autowired
    private UserLoginLogDao userLoginLogDao;

    @Autowired
    private EmployeeService employeeService;

    /**
     * [ 分页查询用户登录日志 ]
     * @param queryDTO [用户名:String + 开始时间:String + 结束时间:String]
     * @return ResponseDTO<PageResultDTO<UserLoginLogDTO>>
     */
    public ResponseDTO<PageResultDTO<UserLoginLogDTO>> queryByPage(UserLoginLogQueryDTO queryDTO) {
        Page page = XiangPageUtil.convert2QueryPage(queryDTO);
        List<UserLoginLogEntity> entities = userLoginLogDao.queryByPage(page, queryDTO);
        List<UserLoginLogDTO> dtoList = XiangBeanUtil.copyList(entities, UserLoginLogDTO.class);
        page.setRecords(dtoList);
        PageResultDTO<UserLoginLogDTO> pageResultDTO = XiangPageUtil.convert2PageResult(page);
        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * [ 删除用户登录日志 ]
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> delete(Long id) {
        userLoginLogDao.deleteById(id);
        return ResponseDTO.succ();
    }

}

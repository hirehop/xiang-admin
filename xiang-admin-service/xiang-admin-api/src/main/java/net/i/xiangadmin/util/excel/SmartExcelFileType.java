package net.i.xiangadmin.util.excel;

/**
 * @author zhuoda
 */
public enum SmartExcelFileType {
    XLS,
    XLSX

}

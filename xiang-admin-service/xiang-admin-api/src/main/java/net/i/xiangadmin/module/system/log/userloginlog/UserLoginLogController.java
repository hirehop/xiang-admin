package net.i.xiangadmin.module.system.log.userloginlog;

import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.constant.SwaggerTagConst;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeQueryDTO;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogDTO;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogQueryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * [ 用户登录日志 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@RestController
@Api(tags = {SwaggerTagConst.Admin.MANAGER_USER_LOGIN_LOG})
public class UserLoginLogController {

    @Autowired
    private UserLoginLogService userLoginLogService;

    @ApiOperation(value = "分页查询用户登录日志")
    @PostMapping("/userLoginLog/page/query")
    public ResponseDTO<PageResultDTO<UserLoginLogDTO>> queryByPage(@RequestBody UserLoginLogQueryDTO queryDTO) {
        return userLoginLogService.queryByPage(queryDTO);
    }

    @ApiOperation(value = "删除用户登录日志")
    @GetMapping("/userLoginLog/delete/{id}")
    public ResponseDTO<String> delete(@PathVariable("id") Long id) {
        return userLoginLogService.delete(id);
    }

}

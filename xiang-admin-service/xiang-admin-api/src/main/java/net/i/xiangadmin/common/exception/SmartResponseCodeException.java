package net.i.xiangadmin.common.exception;

/**
 * [ 状态码异常，全局异常拦截后保留 ResponseCode ]
 *
 * @version 1.0
 * @since JDK1.8
 */
public class SmartResponseCodeException extends RuntimeException{
    private Integer code;

    public SmartResponseCodeException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}

package net.i.xiangadmin.module.system.role.roleprivilege.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * [ 角色权限树DTO（角色ID + 树型权限列表 + 选中的权限） ]
 *
 */
@Data
public class RolePrivilegeTreeDTO {

    @ApiModelProperty("角色ID")
    private Long roleId;

    @ApiModelProperty("权限列表")
    private List<RolePrivilegeSimpleDTO> privilege;

    @ApiModelProperty("选中的权限")
    private List<String> selectedKey;
}

package net.i.xiangadmin.module.system.systemconfig;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.i.xiangadmin.common.constant.JudgeEnum;
import net.i.xiangadmin.common.constant.ResponseCodeConst;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.module.system.systemconfig.constant.SystemConfigDataType;
import net.i.xiangadmin.module.system.systemconfig.constant.SystemConfigEnum;
import net.i.xiangadmin.module.system.systemconfig.constant.SystemConfigResponseCodeConst;
import net.i.xiangadmin.module.system.systemconfig.domain.dto.*;
import net.i.xiangadmin.module.system.systemconfig.domain.entity.SystemConfigEntity;
import net.i.xiangadmin.util.XiangBeanUtil;
import net.i.xiangadmin.util.XiangPageUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * 系统配置业务类
 */
@Slf4j
@Service
public class SystemConfigService {

    /**
     * 系统配置缓存
     */
    private ConcurrentHashMap<String, SystemConfigEntity> systemConfigMap = new ConcurrentHashMap<>();

    @Autowired
    private SystemConfigDao systemConfigDao;

    /**
     * [ 初始化系统设置缓存，配置新增/更新时都会手动调用当前方法更新缓存！ ]
     */
    @PostConstruct
    private void initSystemConfigCache() {
        List<SystemConfigEntity> entityList = systemConfigDao.selectAll();
        if (CollectionUtils.isEmpty(entityList)) {
            return;
        }
        systemConfigMap.clear();
        // 将entity中的configKey转小写后作为键，entity作为值存入systemConfigMap缓存Map中
        entityList.forEach(entity -> this.systemConfigMap.put(entity.getConfigKey().toLowerCase(), entity));
        log.info("系统参数设置缓存(SystemConfigCache)初始化完毕:{}", systemConfigMap.size());
    }

    /**
     * 分页获取系统配置
     *
     * @param queryDTO
     * @return
     */
    public ResponseDTO<PageResultDTO<SystemConfigVO>> getSystemConfigPage(SystemConfigQueryDTO queryDTO) {
        Page page = XiangPageUtil.convert2QueryPage(queryDTO);
        // 将key值转为小写，初始化页面将默认按照key值''查询，这一点会在SQL语句中进行处理！
        if (queryDTO.getKey() != null) {
            queryDTO.setKey(queryDTO.getKey().toLowerCase());
        }
        List<SystemConfigEntity> entityList = systemConfigDao.selectSystemSettingList(page, queryDTO);

        PageResultDTO<SystemConfigVO> pageResultDTO = XiangPageUtil.convert2PageResult(page, entityList, SystemConfigVO.class);
        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * [ 根据SystemConfigEnum.Key获取SystemConfigDTO配置类 ]
     *
     * @param key
     * @return
     */
    public SystemConfigDTO getCacheByKey(SystemConfigEnum.Key key) {
        SystemConfigEntity entity = this.systemConfigMap.get(key.name().toLowerCase());
        if (entity == null) {
            return null;
        }
        return XiangBeanUtil.copy(entity, SystemConfigDTO.class);
    }

    /**
     * 添加系统配置
     *
     * @param configAddDTO
     * @return
     */
    public ResponseDTO<String> addSystemConfig(SystemConfigAddDTO configAddDTO) {
        // 将configKey原有数据转换为小写
        configAddDTO.setConfigKey(configAddDTO.getConfigKey().toLowerCase());
        // 从DB中根据key查找数据
        SystemConfigEntity entity = systemConfigDao.getByKey(configAddDTO.getConfigKey());

        if (entity != null) {
            return ResponseDTO.wrap(SystemConfigResponseCodeConst.ALREADY_EXIST);
        }
        // 验证系统参数值是否符合规范
        ResponseDTO valueValid = this.configValueValid(configAddDTO.getConfigKey(), configAddDTO.getConfigValue());
        if (!valueValid.isSuccess()) {
            return valueValid;
        }
        configAddDTO.setConfigKey(configAddDTO.getConfigKey().toLowerCase());

        // 与DB交互时仍然需要使用Entity，需要额外进行跨类属性拷贝
        SystemConfigEntity addEntity = XiangBeanUtil.copy(configAddDTO, SystemConfigEntity.class);
        addEntity.setIsUsing(JudgeEnum.YES.getValue());
        systemConfigDao.insert(addEntity);

        // 刷新缓存
        this.initSystemConfigCache();
        return ResponseDTO.succ();
    }

    /**
     * 更新系统配置
     *
     * @param updateDTO
     * @return
     */
    public ResponseDTO<String> updateSystemConfig(SystemConfigUpdateDTO updateDTO) {
        updateDTO.setConfigKey(updateDTO.getConfigKey().toLowerCase());
        SystemConfigEntity entity = systemConfigDao.selectById(updateDTO.getId());
        // 系统配置不存在
        if (entity == null) {
            return ResponseDTO.wrap(SystemConfigResponseCodeConst.NOT_EXIST);
        }
        SystemConfigEntity alreadyEntity = systemConfigDao.getByKeyExcludeId(updateDTO.getConfigKey().toLowerCase(), updateDTO.getId());
        if (alreadyEntity != null) {
            return ResponseDTO.wrap(SystemConfigResponseCodeConst.ALREADY_EXIST);
        }
        ResponseDTO valueValid = this.configValueValid(updateDTO.getConfigKey(), updateDTO.getConfigValue());
        if (!valueValid.isSuccess()) {
            return valueValid;
        }
        entity = XiangBeanUtil.copy(updateDTO, SystemConfigEntity.class);
        updateDTO.setConfigKey(updateDTO.getConfigKey().toLowerCase());
        systemConfigDao.updateById(entity);

        // 刷新缓存
        this.initSystemConfigCache();
        return ResponseDTO.succ();
    }


    /**
     * 判断configValue值格式是否符合要求（根据正则表达式模板）
     * 1.如果枚举类中没有当前key，直接验证通过
     * 2.当枚举类中存在时，获取其dataType，进行校验
     *   2.1 获取不到，直接放行
     *   2.2 获取到，根据Regex完成验证，TEXT/JSON不走正则表达式
     * @param configKey
     * @param configValue
     * @return
     */
    private ResponseDTO<String> configValueValid(String configKey, String configValue) {
        SystemConfigEnum.Key configKeyEnum = SystemConfigEnum.Key.selectByKey(configKey);
        if (configKeyEnum == null) {
            return ResponseDTO.succ();
        }
        SystemConfigDataType dataType = configKeyEnum.getDataType();
        if (dataType == null) {
            return ResponseDTO.succ();
        }
        if (dataType.name().equals(SystemConfigDataType.TEXT.name())) {
            return ResponseDTO.succ();
        }
        if (dataType.name().equals(SystemConfigDataType.JSON.name())) {
            try {
                JSONObject jsonStr = JSONObject.parseObject(configValue);
                return ResponseDTO.succ();
            } catch (Exception e) {
                return ResponseDTO.wrap(ResponseCodeConst.ERROR_PARAM, "数据格式不是JSON,请修改后重新提交!");
            }
        }
        if (StringUtils.isNotEmpty(dataType.getValid())) {
            Boolean valid = Pattern.matches(dataType.getValid(), configValue);
            if (valid) {
                return ResponseDTO.succ();
            }
            return ResponseDTO.wrap(ResponseCodeConst.ERROR_PARAM, "数据格式不是" + dataType.name().toLowerCase() + ",请修改后提交。");
        }

        return ResponseDTO.succ();
    }

    /* 目前功能使用不到下述功能 */

    /**
     * 根据参数key获得一条数据（数据库）
     *
     * @param configKey
     * @return
     */
    public ResponseDTO<SystemConfigVO> selectByKey(String configKey) {
        if (configKey != null) {
            configKey = configKey.toLowerCase();
        }
        SystemConfigEntity entity = systemConfigDao.getByKey(configKey);
        if (entity == null) {
            return ResponseDTO.wrap(SystemConfigResponseCodeConst.NOT_EXIST);
        }
        SystemConfigVO configDTO = XiangBeanUtil.copy(entity, SystemConfigVO.class);
        return ResponseDTO.succData(configDTO);
    }

    /**
     * 根据参数key获得一条数据 并转换为 对象
     *
     * @param configKey
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T selectByKey2Obj(String configKey, Class<T> clazz) {
        if (configKey != null) {
            configKey = configKey.toLowerCase();
        }
        SystemConfigEntity entity = systemConfigDao.getByKey(configKey);
        if (entity == null) {
            return null;
        }
        SystemConfigDTO configDTO = XiangBeanUtil.copy(entity, SystemConfigDTO.class);
        String configValue = configDTO.getConfigValue();
        if (StringUtils.isEmpty(configValue)) {
            return null;
        }
        T obj = JSON.parseObject(configValue, clazz);
        return obj;
    }

    /**
     * 根据分组名称 获取获取系统设置
     *
     * @param group
     * @return
     */
    public ResponseDTO<List<SystemConfigVO>> getListByGroup(String group) {

        List<SystemConfigEntity> entityList = systemConfigDao.getListByGroup(group);
        if (CollectionUtils.isEmpty(entityList)) {
            return ResponseDTO.succData(Lists.newArrayList());
        }
        List<SystemConfigVO> systemConfigList = XiangBeanUtil.copyList(entityList, SystemConfigVO.class);
        return ResponseDTO.succData(systemConfigList);
    }

    /**
     * 根据分组名称 获取获取系统设置
     *
     * @param group
     * @return
     */
    public List<SystemConfigDTO> getListByGroup(SystemConfigEnum.Group group) {
        List<SystemConfigEntity> entityList = systemConfigDao.getListByGroup(group.name());
        if (CollectionUtils.isEmpty(entityList)) {
            return Lists.newArrayList();
        }
        List<SystemConfigDTO> systemConfigList = XiangBeanUtil.copyList(entityList, SystemConfigDTO.class);
        return systemConfigList;
    }

}

package net.i.xiangadmin.module.system.log;

import lombok.extern.slf4j.Slf4j;
import net.i.xiangadmin.module.system.log.userloginlog.UserLoginLogDao;
import net.i.xiangadmin.module.system.log.userloginlog.domain.UserLoginLogEntity;
import net.i.xiangadmin.util.XiangThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * [ 日志服务，对外提供给登录接口 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Slf4j
@Service
public class LogService {

    private ThreadPoolExecutor threadPoolExecutor;

    @Autowired
    private UserLoginLogDao userLoginLogDao;

    @PostConstruct
    void init() {
        if (threadPoolExecutor == null) {
            threadPoolExecutor = new ThreadPoolExecutor(1, 10, 10L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(2000), XiangThreadFactory.create("LogAspect"));
        }
    }

    @PreDestroy
    void destroy() {
        if (threadPoolExecutor != null) {
            threadPoolExecutor.shutdown();
            threadPoolExecutor = null;
        }
    }

    public void addLog(Object object) {
        try {
            // 用户登录日志
            if (object instanceof UserLoginLogEntity) {
                threadPoolExecutor.execute(() -> userLoginLogDao.insert((UserLoginLogEntity) object));
            }else{
                log.error("不支持添加当前类型日志，请联系系统管理员！");
            }
        } catch (Throwable e) {
            log.error("日志添加异常，请联系系统管理员！");
        }
    }
}

package net.i.xiangadmin.module.business.file.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.module.business.file.FileDao;
import net.i.xiangadmin.module.business.file.constant.FileServiceTypeEnum;
import net.i.xiangadmin.module.business.file.constant.FileTypeEnum;
import net.i.xiangadmin.module.business.file.domain.dto.FileAddDTO;
import net.i.xiangadmin.module.business.file.domain.dto.FileDTO;
import net.i.xiangadmin.module.business.file.domain.dto.FileQueryDTO;
import net.i.xiangadmin.module.business.file.domain.entity.FileEntity;
import net.i.xiangadmin.module.business.file.domain.vo.FileVO;
import net.i.xiangadmin.module.business.file.domain.vo.UploadVO;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import net.i.xiangadmin.util.SmartRequestTokenUtil;
import net.i.xiangadmin.util.XiangBeanUtil;
import net.i.xiangadmin.util.XiangPageUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * [ 文件Service层 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Service
public class FileService {

    @Autowired
    private FileDao fileDao;

    /* 注入Map，可以直接将泛型值多个注入时自动封装为Map */
    @Autowired
    private java.util.Map<String, IFileService> fileServiceMap;

    /**
     * 获取文件服务实现
     *
     * @param typeEnum
     * @return
     */
    private IFileService getFileService(FileServiceTypeEnum typeEnum) {
        /**
         * 获取文件服务
         */
        String serviceName = typeEnum.getServiceName();
        IFileService fileService = fileServiceMap.get(serviceName);
        if (null == fileService) {
            throw new RuntimeException("未找到文件服务实现类：" + serviceName);
        }
        return fileService;
    }

    /**
     * 文件上传服务
     *
     * @param file
     * @param typeEnum
     * @param filePath
     * @return
     */
    public ResponseDTO<UploadVO> fileUpload(MultipartFile file, FileServiceTypeEnum typeEnum, String filePath,
                                            Integer fileType, Long courseId) {
        // 从fileServiceMap中获取文件服务
        IFileService fileService = this.getFileService(typeEnum);
        ResponseDTO<UploadVO> response = fileService.fileUpload(file, filePath, fileType, courseId);
        return response;
    }

    /**
     * 批量插入
     *
     * @param fileDTOList
     */
    public void insertFileBatch(List<FileDTO> fileDTOList) {
        fileDao.insertFileBatch(fileDTOList);
    }

    /**
     * @param filesStr 逗号分隔文件id字符串
     * @return
     */
    public List<FileVO> getFileDTOList(String filesStr) {
        if (StringUtils.isEmpty(filesStr)) {
            return Lists.newArrayList();
        }
        String[] fileIds = filesStr.split(",");
        List<Long> fileIdList = Arrays.asList(fileIds).stream().map(e -> Long.valueOf(e)).collect(Collectors.toList());
        List<FileVO> files = fileDao.listFilesByFileIds(fileIdList);
        return files;
    }

    /**
     * 分页查询文件列表
     *
     * @param queryDTO
     * @return
     */
    public ResponseDTO<PageResultDTO<FileVO>> queryListByPage(FileQueryDTO queryDTO, HttpServletRequest request) {
        Page page = XiangPageUtil.convert2QueryPage(queryDTO);

        // 将fileType值映射为desc
        if(StringUtils.isNotEmpty(queryDTO.getFileType())) {
            queryDTO.setFileType(FileTypeEnum.getDescFromType(Integer.valueOf(queryDTO.getFileType())));
        }
        // 获取当前登录用户ID，用户仅仅能够查看到自己上传的文件（管理员除外）
        Long requestUserId = ((RequestTokenDTO) (request.getAttribute(SmartRequestTokenUtil.USER_KEY))).getRequestUserId();
        List<FileVO> fileList = fileDao.queryListByPage(page, queryDTO);
        if(!requestUserId.equals(1L)){
            fileList = fileList.stream().filter(x->x.getCreateUser().equals(requestUserId)).collect(Collectors.toList());
        }

        PageResultDTO<FileVO> pageResultDTO = XiangPageUtil.convert2PageResult(page, fileList);
        return ResponseDTO.succData(pageResultDTO);
    }

    /**
     * 根据id 下载文件
     *
     * @param id
     * @param request
     * @return
     */
    public ResponseEntity<byte[]> downLoadById(Long id, HttpServletRequest request) {
        FileEntity entity = fileDao.selectById(id);
        if (null == entity) {
            throw new RuntimeException("文件信息不存在");
        }
        // 文件服务类型默认设置为Local
        FileServiceTypeEnum serviceTypeEnum = FileServiceTypeEnum.LOCAL;
        IFileService fileService = this.getFileService(serviceTypeEnum);
        ResponseEntity<byte[]> stream = fileService.fileDownload(entity.getFilePath(), entity.getFileName(), request);
        return stream;
    }

    /**
     * 根据ID删除文件
     *
     * @param id
     * @param request
     * @return
     */
    public ResponseDTO<String>  deleteById(Long id, HttpServletRequest request) {
        FileEntity entity = fileDao.selectById(id);
        if (null == entity) {
            throw new RuntimeException("文件信息不存在");
        }
        // 文件服务类型默认设置为Local
        FileServiceTypeEnum serviceTypeEnum = FileServiceTypeEnum.LOCAL;
        IFileService fileService = this.getFileService(serviceTypeEnum);
        // 数据库中删除记录
        fileDao.deleteById(id);
        return fileService.fileDelete(entity.getFilePath(), entity.getFileName(), request);
    }

    /**
     * 系统文件保存
     * @param addDTO
     * @return
     */
    public ResponseDTO<String> saveFile(FileAddDTO addDTO, RequestTokenDTO requestToken) {
        FileEntity entity = XiangBeanUtil.copy(addDTO,FileEntity.class);
        // 文件大小 + 文件类型（类型通过枚举映射为desc，将desc写入数据库）
        entity.setFileSize(Integer.parseInt(addDTO.getFileSize())/1024 + "KB");
        entity.setFileType(FileTypeEnum.getDescFromType(addDTO.getFileType()));
        entity.setCreateUser(requestToken.getRequestUserId());
        entity.setCreateTime(new Date());
        fileDao.insert(entity);
        return ResponseDTO.succ();
    }
}

package net.i.xiangadmin.module.system.login.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * [ 登录返回DTO (员工基本信息 + 超管 + 权限集合 + 部门名称 + token) ]
 *
 */
@Data
public class LoginDetailDTO {

    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("登录账号")
    private String loginName;

    @ApiModelProperty("员工名称")
    private String name;

    @ApiModelProperty("手机号码")
    private String phone;

    @ApiModelProperty("出生日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birthday;

    @ApiModelProperty("创建者id")
    private Long createUser;

    @ApiModelProperty("部门id")
    private Long departmentId;

    @ApiModelProperty("是否离职")
    private Integer isLeave;

    @ApiModelProperty("是否被禁用")
    private Integer isDisabled;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("性别")
    private String gender;

    @ApiModelProperty("学历")
    private String degree;

    @ApiModelProperty("备注")
    private String remark;

    /* ADD-1 */
    @ApiModelProperty("部门名称")
    private String departmentName;

    /* ADD-2 */
    @ApiModelProperty("登陆token")
    private String xAccessToken;

    @ApiModelProperty("是否为超管")
    private Boolean isSuperMan;

    @ApiModelProperty("权限列表")
    private List<LoginPrivilegeDTO> privilegeList;

}

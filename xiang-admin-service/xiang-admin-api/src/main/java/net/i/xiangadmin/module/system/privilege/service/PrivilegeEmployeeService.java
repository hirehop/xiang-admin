package net.i.xiangadmin.module.system.privilege.service;

import net.i.xiangadmin.common.constant.JudgeEnum;
import net.i.xiangadmin.common.exception.SmartBusinessException;
import net.i.xiangadmin.module.system.employee.domain.dto.EmployeeDTO;
import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import net.i.xiangadmin.module.system.privilege.constant.PrivilegeTypeEnum;
import net.i.xiangadmin.module.system.privilege.dao.PrivilegeDao;
import net.i.xiangadmin.module.system.privilege.domain.entity.PrivilegeEntity;
import net.i.xiangadmin.module.system.role.roleemployee.RoleEmployeeDao;
import net.i.xiangadmin.module.system.role.roleprivilege.RolePrivilegeDao;
import net.i.xiangadmin.module.system.systemconfig.SystemConfigService;
import net.i.xiangadmin.module.system.systemconfig.constant.SystemConfigEnum;
import net.i.xiangadmin.module.system.systemconfig.domain.dto.SystemConfigDTO;
import net.i.xiangadmin.util.SmartStringUtil;
import com.google.common.collect.Lists;
import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * [ 后台员工权限缓存方法 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Service
public class PrivilegeEmployeeService {

    /**
     * 后台用户权限缓存 <id,<controllerName,methodName></>></>
     *
     * maximumWeightedCapacity？
     *     - ConcurrentLinkedHashMap是java.util.LinkedHashMap的一个高性能实现。主要用于软件缓存。
     *     - ConcurrentLinkedHashMap 是google团队提供的一个容器。
     *     - 其实它本身是对ConcurrentHashMap的封装，可以用来实现一个基于LRU策略的缓存。
     */
    private ConcurrentMap<Long, Map<String, List<String>>> employeePrivileges = new ConcurrentLinkedHashMap.Builder<Long, Map<String, List<String>>>().maximumWeightedCapacity(1000).build();
    private ConcurrentMap<Long, List<PrivilegeEntity>> employeePrivilegeListMap = new ConcurrentLinkedHashMap.Builder<Long, List<PrivilegeEntity>>().maximumWeightedCapacity(1000).build();

    @Autowired
    private SystemConfigService systemConfigService;

    @Autowired
    private RoleEmployeeDao roleEmployeeDao;

    @Autowired
    private RolePrivilegeDao rolePrivilegeDao;

    @Autowired
    private PrivilegeDao privilegeDao;

    /**
     * [ 根据用户ID，判断是否为超级管理员；该属性+EmployeeEntity共同初始化EmployeeBO! ]
     *
     * @param employeeId 员工ID
     * @return
     */
    public Boolean isSuperman(Long employeeId) {
        SystemConfigDTO systemConfig = systemConfigService.getCacheByKey(SystemConfigEnum.Key.EMPLOYEE_SUPERMAN);
        if (systemConfig == null) {
            throw new SmartBusinessException("缺少系统配置项[" + SystemConfigEnum.Key.EMPLOYEE_SUPERMAN.name() + "]");
        }
        List<Long> superManIdsList = SmartStringUtil.splitConverToLongList(systemConfig.getConfigValue(), ",");
        return superManIdsList.contains(employeeId);
    }


    /**
     * 移除某人缓存中的权限
     *
     * @param employeeId
     */
    public void removeCache(Long employeeId) {
        this.employeePrivileges.remove(employeeId);
    }

    /**
     * 检查某人是否有访问某个方法的权限
     *
     * @param requestTokenDTO
     * @param controllerName
     * @param methodName
     * @return
     */
    public Boolean checkEmployeeHavePrivilege(RequestTokenDTO requestTokenDTO, String controllerName, String methodName) {
        if (StringUtils.isEmpty(controllerName) || StringUtils.isEmpty(methodName)) {
            return false;
        }
        // 优先考虑用户是否为超级管理员
        Boolean isSuperman = requestTokenDTO.getEmployeeBO().getIsSuperman();

        if (isSuperman) {
            return true;
        }

        // 根据token获取指定用户的ID，根据id获取当前用户的所有权限集合
        //     - 键：控制器名
        //     - 值：url集合
        Map<String, List<String>> privileges = this.getPrivileges(requestTokenDTO.getRequestUserId());
        // 获取当前用户所有的权限集合
        List<String> urlList = privileges.get(controllerName.toLowerCase());
        if (CollectionUtils.isEmpty(urlList)) {
            return false;
        }
        return urlList.contains(methodName);
    }

    public List<PrivilegeEntity> getEmployeeAllPrivilege(Long employeeId) {
        return employeePrivilegeListMap.get(employeeId);
    }


    /**
     * 根据员工ID 获取 权限信息
     *
     * @param employeeId
     * @return
     */
    public List<PrivilegeEntity> getPrivilegesByEmployeeId(Long employeeId) {
        List<PrivilegeEntity> privilegeEntities = null;
        // 超管
        Boolean isSuperman = this.isSuperman(employeeId);
        if (isSuperman) {
            privilegeEntities = privilegeDao.selectAll();
        } else {
            privilegeEntities = loadPrivilegeFromDb(employeeId);
        }

        if (privilegeEntities == null) {
            privilegeEntities = Lists.newArrayList();
        }

        this.updateCachePrivilege(employeeId, privilegeEntities);
        return privilegeEntities;
    }

    /**
     * 获取某人所能访问的方法
     *
     * @param employeeId
     * @return
     */
    private Map<String, List<String>> getPrivileges(Long employeeId) {
        // 首先尝试从缓存中进行读取指定权限
        Map<String, List<String>> privileges = employeePrivileges.get(employeeId);
        if (privileges != null) {
            return privileges;
        }
        // 缓存中不存在时，从数据库中加载权限
        List<PrivilegeEntity> privilegeEntities = this.loadPrivilegeFromDb(employeeId);

        // 更新缓存中的权限信息并返回employeePrivileges（权限缓存Map）
        return updateCachePrivilege(employeeId, privilegeEntities);
    }

    /**
     * 根据员工ID从数据库中加载所有权限
     * @param employeeId
     * @return
     */
    private List<PrivilegeEntity> loadPrivilegeFromDb(Long employeeId) {
        // 获取当前用户的所有角色
        List<Long> roleIdList = roleEmployeeDao.selectRoleIdByEmployeeId(employeeId);
        if (CollectionUtils.isEmpty(roleIdList)) {
            return Lists.newArrayList();
        }
        // 获取角色的所有权限，映射为PrivilegeEntity实体
        List<PrivilegeEntity> privilegeEntities = rolePrivilegeDao.listByRoleIds(roleIdList, JudgeEnum.YES.getValue());
        if (privilegeEntities != null) {
            return privilegeEntities;
        }
        return Collections.emptyList();
    }

    /**
     * [ 重要：更新本地缓存信息 ]
     *
     * @param employeeId 员工ID
     * @param privilegeEntities 权限实体集合（ URL此处描述为ControllerName.MethodName ）
     * @return
     */
    public Map<String, List<String>> updateCachePrivilege(Long employeeId, List<PrivilegeEntity> privilegeEntities) {
        employeePrivilegeListMap.put(employeeId, privilegeEntities);
        // url列表
        List<String> privilegeList = new ArrayList<>();
        // HashMap默认大小为16（2<<4）
        Map<String, List<String>> privilegeMap = new HashMap<>(16);
        if (CollectionUtils.isNotEmpty(privilegeEntities)) {
            // 每个元素为urlList（权限中的URI可以配置多个，使用逗号分隔）
            List<List<String>> setList =
                    privilegeEntities.stream().filter(e -> e.getType().equals(PrivilegeTypeEnum.POINTS.getValue())).map(PrivilegeEntity::getUrl).collect(Collectors.toList()).stream().map(e -> SmartStringUtil.splitConvertToList(e, ",")).collect(Collectors.toList());
            // 将所有URI拆分添加到privilegeList
            setList.forEach(privilegeList::addAll);
        }
        privilegeList.forEach(item -> {
            List<String> path = SmartStringUtil.splitConvertToList(item, "\\.");
            String controllerName = path.get(0).toLowerCase();
            String methodName = path.get(1);
            List<String> methodNameList = privilegeMap.get(controllerName);
            if (null == methodNameList) {
                methodNameList = new ArrayList();
            }
            if (!methodNameList.contains(methodName)) {
                methodNameList.add(methodName);
            }
            privilegeMap.put(controllerName, methodNameList);
        });

        employeePrivileges.put(employeeId, privilegeMap);
        return privilegeMap;
    }

    public void updateOnlineEmployeePrivilegeByRoleId(Long roleId) {
        List<EmployeeDTO> roleEmployeeList = roleEmployeeDao.selectEmployeeByRoleId(roleId);
        List<Long> employeeIdList = roleEmployeeList.stream().map(e -> e.getId()).collect(Collectors.toList());

        for (Long empId : employeePrivileges.keySet()) {
            if (employeeIdList.contains(empId)) {
                getPrivilegesByEmployeeId(empId);
            }
        }
    }
}

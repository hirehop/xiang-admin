package net.i.xiangadmin.module.business.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.i.xiangadmin.module.business.course.domain.dto.TCourseQueryDTO;
import net.i.xiangadmin.module.business.course.domain.entity.TCourseEntity;
import net.i.xiangadmin.module.business.course.domain.vo.TCourseVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * [ ${tableDesc} ]
 *
 * @author X
 * @version 1.0
 * @company ${company}
 * @copyright (c)  ${company}Inc. All rights reserved.
 * @date 2021-04-18 14:12:50
 * @since JDK1.8
 */
@Mapper
@Component
public interface TCourseDao extends BaseMapper<TCourseEntity> {

    /**
     * 分页查询
     *
     * @param queryDTO
     * @return TCourseVO
     */
    IPage<TCourseVO> queryByPage(Page page, @Param("queryDTO") TCourseQueryDTO queryDTO);

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    void deleteById(@Param("id") Long id);

    /**
     * 根据id批量删除
     *
     * @param idList
     * @return
     */
    void deleteByIdList(@Param("idList") List<Long> idList);
}

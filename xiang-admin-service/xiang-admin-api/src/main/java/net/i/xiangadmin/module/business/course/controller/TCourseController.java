package net.i.xiangadmin.module.business.course.controller;

import io.lettuce.core.dynamic.annotation.Param;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.i.xiangadmin.common.controller.BaseController;
import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.common.domain.ValidateList;
import net.i.xiangadmin.common.domain.page.PageResultDTO;
import net.i.xiangadmin.module.business.course.domain.dto.TCourseAddDTO;
import net.i.xiangadmin.module.business.course.domain.dto.TCourseQueryDTO;
import net.i.xiangadmin.module.business.course.domain.dto.TCourseUpdateDTO;
import net.i.xiangadmin.module.business.course.domain.vo.TCourseVO;
import net.i.xiangadmin.module.business.course.service.TCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * [ ${tableDesc} ]
 *
 * @author X
 * @version 1.0
 * @since JDK1.8
 */
@RestController
@Api(tags = {"${tableDesc}"})
public class TCourseController extends BaseController {

    @Autowired
    private TCourseService tCourseService;

    @ApiOperation(value = "分页查询${tableDesc}", notes = "@author X")
    @PostMapping("/tCourse/page/query")
    public ResponseDTO<PageResultDTO<TCourseVO>> queryByPage(@RequestBody TCourseQueryDTO queryDTO, HttpServletRequest request) {
        return tCourseService.queryByPage(queryDTO, request);
    }

    @ApiOperation(value = "添加${tableDesc}", notes = "@author X")
    @PostMapping("/tCourse/add")
    public ResponseDTO<String> add(@RequestBody @Validated TCourseAddDTO addTO) {
        return tCourseService.add(addTO);
    }

    @ApiOperation(value = "修改${tableDesc}", notes = "@author X")
    @PostMapping("/tCourse/update")
    public ResponseDTO<String> update(@RequestBody @Validated TCourseUpdateDTO updateDTO) {
        return tCourseService.update(updateDTO);
    }

    @ApiOperation(value = "根据Id删除课程")
    @GetMapping("/tCourse/delete")
    public ResponseDTO<String> deleteById(@Param("id") Long id, HttpServletRequest request) {
        return tCourseService.deleteById(id, request);
    }

    @ApiOperation(value = "批量删除${tableDesc}", notes = "@author X")
    @PostMapping("/tCourse/deleteByIds")
    public ResponseDTO<String> delete(@RequestBody @Validated ValidateList<Long> idList) {
        return tCourseService.deleteByIds(idList);
    }

}

package net.i.xiangadmin.module.system.login.domain;

import net.i.xiangadmin.module.system.employee.domain.bo.EmployeeBO;
import lombok.Getter;


@Getter
public class RequestTokenDTO {

    private Long requestUserId;

    private EmployeeBO employeeBO;

    public RequestTokenDTO(EmployeeBO employeeBO) {
        this.requestUserId = employeeBO.getId();
        this.employeeBO = employeeBO;
    }

    @Override
    public String toString() {
        return "RequestTokenDTO{" +
                "requestUserId=" + requestUserId +
                ", employeeBO=" + employeeBO +
                '}';
    }
}

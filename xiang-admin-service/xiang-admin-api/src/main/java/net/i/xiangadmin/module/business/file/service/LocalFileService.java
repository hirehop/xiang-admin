package net.i.xiangadmin.module.business.file.service;

import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.module.business.file.constant.FileResponseCodeConst;
import net.i.xiangadmin.module.business.file.constant.FileServiceNameConst;
import net.i.xiangadmin.module.business.file.constant.FileTypeEnum;
import net.i.xiangadmin.module.business.file.domain.vo.UploadVO;
import net.i.xiangadmin.module.system.systemconfig.SystemConfigDao;
import net.i.xiangadmin.module.system.systemconfig.constant.SystemConfigEnum;
import net.i.xiangadmin.module.system.systemconfig.domain.entity.SystemConfigEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * [ 本地文件服务 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Slf4j
@Service(FileServiceNameConst.LOCAL)
public class LocalFileService implements IFileService {

    @Autowired
    private SystemConfigDao systemConfigDao;

    @Value("${spring.servlet.multipart.max-file-size}")
    private String maxFileSize;

    @Value("${file-upload-service.path}")
    private String fileParentPath;

    private static final Long DEFAULT_SIZE = 10 * 1024 * 1024L;

    /**
     * [ 文件上传 ]
     *
     * @param multipartFile
     * @param path
     * @param fileType
     * @return
     */
    @Override
    public ResponseDTO<UploadVO> fileUpload(MultipartFile multipartFile, String path, Integer fileType, Long courseId) {
        if (null == multipartFile) {
            return ResponseDTO.wrap(FileResponseCodeConst.FILE_EMPTY);
        }
        Long maxSize = DEFAULT_SIZE;
        if (StringUtils.isNotEmpty(maxFileSize)) {
            String maxSizeStr = maxFileSize.toLowerCase().replace("mb", "");
            maxSize = Integer.valueOf(maxSizeStr) * 1024 * 1024L;
        }
        if (multipartFile.getSize() > maxSize) {
            return ResponseDTO.wrap(FileResponseCodeConst.FILE_SIZE_ERROR, String.format(FileResponseCodeConst.FILE_SIZE_ERROR.getMsg(), maxFileSize));
        }

        // 定义文件路径
        String filePath = fileParentPath;
        if (StringUtils.isNotEmpty(path)) {
            // filePath => /home/upload/file/teacher
            filePath = filePath + path + "/";
        }

        File directory = new File(filePath);
        if (!directory.exists()) {
            // 目录不存在，新建
            directory.mkdirs();
        }
        // 文件名 + 文件路径 + 文件大小
        UploadVO localUploadVO = new UploadVO();
        // 获取原文件名 + 根据原文件名生成新的文件名
        String originalFileName = multipartFile.getOriginalFilename();
        String newFileName = this.generateFileName(originalFileName);

        // 获取绝对路径并根据该路径创建文件
        // 默认C盘根目录
        File fileTemp = new File(new File(filePath + newFileName).getAbsolutePath());
        try {
            // 将MutipartFile转换为File，并创建文件
            multipartFile.transferTo(fileTemp);
            localUploadVO.setFileName(newFileName);
            localUploadVO.setFilePath(path + "/" + newFileName);
            localUploadVO.setFileSize(multipartFile.getSize());
            localUploadVO.setCourseId(courseId);
            // 传递给前端一个枚举类型（没有直接将数据传递给前端）
            localUploadVO.setFileType(fileType);
        } catch (IOException e) {
            // 出错时，删掉正在写入的文件
            if (fileTemp.exists() && fileTemp.isFile()) {
                fileTemp.delete();
            }
            log.error("", e);
            return ResponseDTO.wrap(FileResponseCodeConst.UPLOAD_ERROR);
        }
        return ResponseDTO.succData(localUploadVO);
    }

    @Override
    public ResponseEntity<byte[]> fileDownload(String key, String fileName, HttpServletRequest request) {

        String url = fileParentPath + key;
        // 创建文件
        File file = new File(url);
        return this.downloadMethod(file, request);
    }

    /**
     * 删除本地文件
     *
     * @param key
     * @param fileName
     * @param request
     * @return
     */
    public ResponseDTO<String> fileDelete(String key, String fileName, HttpServletRequest request){
        String url = fileParentPath + key;
        File file = new File(url);
        boolean resFlag = file.delete();
        return resFlag?ResponseDTO.succ():ResponseDTO.wrap(FileResponseCodeConst.DELETE_ERROR);
    }

}

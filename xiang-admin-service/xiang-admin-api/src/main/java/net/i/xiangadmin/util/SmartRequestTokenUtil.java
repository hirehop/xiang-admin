package net.i.xiangadmin.util;

import net.i.xiangadmin.module.system.login.domain.RequestTokenDTO;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
/**
 *
 * @version 1.0
 * @since JDK1.8
 */
public class SmartRequestTokenUtil {

    public static final String USER_KEY = "xiang_admin_user";

    private static ThreadLocal<RequestTokenDTO> RequestUserThreadLocal = new ThreadLocal<RequestTokenDTO>();

    public static void setUser(HttpServletRequest request, RequestTokenDTO requestToken) {
        request.setAttribute(USER_KEY, requestToken);
        RequestUserThreadLocal.set(requestToken);
    }

    public static RequestTokenDTO getThreadLocalUser() {
        return RequestUserThreadLocal.get();
    }

    /**
     * [ 从请求域中获取指定键的token对象：RequestTokenDTO ]
     *
     * @return
     */
    public static RequestTokenDTO getRequestUser() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            if (request != null) {
                return (RequestTokenDTO) request.getAttribute(USER_KEY);
            }
        }
        return null;
    }

    public static Long getRequestUserId() {
        RequestTokenDTO requestUser = getRequestUser();
        if (null == requestUser) {
            return null;
        }
        return requestUser.getRequestUserId();
    }

}

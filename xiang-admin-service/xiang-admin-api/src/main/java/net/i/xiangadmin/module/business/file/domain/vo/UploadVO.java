package net.i.xiangadmin.module.business.file.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class UploadVO {

    @ApiModelProperty(value = "文件名称")
    private String fileName;
    @ApiModelProperty(value = "filePath")
    private String filePath;
    @ApiModelProperty(value = "文件大小")
    private Long fileSize;


    @ApiModelProperty(value = "文件类型")
    private Integer fileType;
    @ApiModelProperty(value = "所属课程ID")
    private Long courseId;
}

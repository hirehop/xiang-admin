package net.i.xiangadmin.module.system.systemconfig.constant;

import net.i.xiangadmin.util.XiangVerificationUtil;

/**
 * [  ]
 *
 * @version 1.0
 * @since JDK1.8
 */
public enum SystemConfigDataType {
    /**
     * 整数
     */
    INTEGER(XiangVerificationUtil.INTEGER),
    /**
     * 文本
     */
    TEXT(null),
    /**
     * url地址
     */
    URL(XiangVerificationUtil.URL),
    /**
     *  邮箱
     */
    EMAIL(XiangVerificationUtil.EMAIL),
    /**
     * JSON 字符串
     */
    JSON(null),
    /**
     * 2019-08
     */
    YEAR_MONTH(XiangVerificationUtil.YEAR_MONTH),
    /**
     * 2019-08-01
     */
    DATE(XiangVerificationUtil.DATE),
    /**
     * 2019-08-01 10:23
     */
    DATE_TIME(XiangVerificationUtil.DATE_TIME),
    /**
     * 10:23-10:56
     */
    TIME_SECTION(XiangVerificationUtil.TIME_SECTION),
    /**
     * 10:23
     */
    TIME(XiangVerificationUtil.TIME);

    private String valid;

    SystemConfigDataType(String valid){
        this.valid = valid;
    }

    public String getValid() {
        return valid;
    }
}

package net.i.xiangadmin.common.exception;
/**
 * 
 * [ 业务逻辑异常,全局异常拦截后统一返回ResponseCodeConst.SYSTEM_ERROR ]
 * 
 * @version 1.0
 * @since JDK1.8
 */
public class SmartBusinessException extends RuntimeException {

    public SmartBusinessException() {
    }

    public SmartBusinessException(String message) {
        super(message);
    }
}

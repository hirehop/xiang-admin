package net.i.xiangadmin.module.business.file.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Date;

/**
 * [ FileVO，值对象，用于MyBatis数据映射，DTO用于数据传输，将二者在逻辑中分开 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class FileVO {

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("文件名称")
    private String fileName;

    @ApiModelProperty("文件大小")
    private String fileSize;

    @ApiModelProperty("文件类型")
    private String fileType;

    @ApiModelProperty("文件路径")
    private String filePath;

    @ApiModelProperty("上传人")
    private Long createUser;

    @ApiModelProperty("所属课程ID")
    private Long courseId;

    @ApiModelProperty("虚拟字段 - 所属课程名称")
    private String name;

    @ApiModelProperty("updateTime")
    private Date updateTime;

    @ApiModelProperty("创建时间")
    private Date createTime;

}

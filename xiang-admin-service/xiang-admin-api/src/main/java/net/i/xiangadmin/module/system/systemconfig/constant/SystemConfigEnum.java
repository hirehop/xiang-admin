package net.i.xiangadmin.module.system.systemconfig.constant;


import java.util.Arrays;
import java.util.Optional;

/**
 * [ 系统配置常量类 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
public class SystemConfigEnum {

    public enum Group {
        BACK,
        GIT_LOG
    }

    public enum Key {
        /**
         * 超管id
         */
        EMPLOYEE_SUPERMAN(SystemConfigDataType.TEXT),
        /**
         * 本地文件上传url前缀（"aaa/bbb，开头不能使用左斜线分隔！"）
         */
        LOCAL_UPLOAD_URL_PREFIX(SystemConfigDataType.URL);

        private SystemConfigDataType dataType;

        Key(SystemConfigDataType dataType) {
            this.dataType = dataType;
        }


        public SystemConfigDataType getDataType() {
            return dataType;
        }

        public static Key selectByKey(String key) {
            Key[] values = Key.values();
            Optional<Key> first = Arrays.stream(values).filter(e -> e.name().equalsIgnoreCase(key)).findFirst();
            return !first.isPresent() ? null : first.get();
        }
    }

}

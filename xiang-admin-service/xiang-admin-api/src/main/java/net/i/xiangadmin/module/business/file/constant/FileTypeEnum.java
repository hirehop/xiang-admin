package net.i.xiangadmin.module.business.file.constant;

import net.i.xiangadmin.common.constant.BaseEnum;
import org.apache.commons.lang3.StringUtils;

public enum FileTypeEnum implements BaseEnum {
    A(1,"教学大纲"),
    B(2,"教学日历"),
    C(3,"教学小结");

    private Integer fileType;
    private String fileTypeDesc;

    FileTypeEnum(Integer fileType, String fileTypeDesc){
        this.fileType = fileType;
        this.fileTypeDesc = fileTypeDesc;
    }

    @Override
    public Integer getValue() {
        return fileType;
    }

    @Override
    public String getDesc() {
        return fileTypeDesc;
    }

    /**
     * 根据文件类型获取desc（desc将写入数据库）
     *
     * @param fileType
     * @return
     */
    public static String getDescFromType(Integer fileType){
        int type = 0;
        if(fileType != null){
            type = fileType;
        }
        switch(type){
            case 1:
                return A.getDesc();
            case 2:
                return B.getDesc();
            case 3:
                return C.getDesc();
            default:
                return "";
        }
    }

    /**
     * 由于数据库中存储的是文件的desc，这里需要根据desc将其转换为type
     *
     * @param desc
     * @return
     */
    public static Integer getTypeFromDesc(String desc){
        if(StringUtils.isNotEmpty(desc)){
            switch(desc){
                case "文件类型A":
                    return A.getValue();
                case "文件类型B":
                    return B.getValue();
                case "文件类型C":
                    return C.getValue();
                default:
                    return 0;
            }
        }
        return 0;
    }
}

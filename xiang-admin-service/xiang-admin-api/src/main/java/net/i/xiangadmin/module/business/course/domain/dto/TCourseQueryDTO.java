package net.i.xiangadmin.module.business.course.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.i.xiangadmin.common.domain.page.PageParamDTO;

/**
 * [ Course查询DTO ]
 *
 * @author X
 * @version 1.0
 * @since JDK1.8
 */
@Data
public class TCourseQueryDTO extends PageParamDTO {
    @ApiModelProperty("课程名")
    String name;

    @ApiModelProperty("员工Id,隐藏字段-从token中获取")
    Long employeeId;

    @ApiModelProperty("工作日（星期N），可以多个，使用半角分号分隔")
    private String workWeekday;

    @ApiModelProperty("工作时间段，可以多个，一天内使用半角逗号分隔，不同天使用半角分号分隔")
    private String workTimePart;

    @ApiModelProperty("课程所属学年（2016-2017）")
    private String workYear;

    @ApiModelProperty("课程所属学期（1/2）")
    private Integer workPart;

    @ApiModelProperty("用户所要查询的周数")
    private Integer workWeek;
}

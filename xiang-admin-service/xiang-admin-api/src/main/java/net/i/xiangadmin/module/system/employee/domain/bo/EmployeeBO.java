package net.i.xiangadmin.module.system.employee.domain.bo;

import lombok.Getter;
import net.i.xiangadmin.module.system.employee.domain.entity.EmployeeEntity;


/**
 * 基本业务字段(名称+手机号) + 是否为超级管理员
 */
@Getter
public class EmployeeBO {

    /**
     * 主键id
     */
    private Long id;

    /**
     * 登录账号
     */
    private String loginName;

    /**
     * 员工名称
     */
    private String name;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 部门id
     */
    private Long departmentId;

    /**
     * 是否离职
     */
    private Integer isLeave;

    /**
     * 是否被禁用
     */
    private Integer isDisabled;

    /**
     * 删除状态 0否 1是
     */
    private Long isDelete;

    /**
     * 是否为超级管理员
     */
    private Boolean isSuperman;

    public EmployeeBO(EmployeeEntity employeeEntity, boolean isSuperman) {
        this.id = employeeEntity.getId();
        this.loginName = employeeEntity.getLoginName();
        this.name = employeeEntity.getName();
        this.phone = employeeEntity.getPhone();
        this.departmentId = employeeEntity.getDepartmentId();
        this.isLeave = employeeEntity.getIsLeave();
        this.isDisabled = employeeEntity.getIsDisabled();
        this.isDelete = employeeEntity.getIsDelete();
        this.isSuperman = isSuperman;
    }

}

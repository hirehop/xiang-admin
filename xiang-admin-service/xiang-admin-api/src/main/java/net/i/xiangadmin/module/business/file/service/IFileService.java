package net.i.xiangadmin.module.business.file.service;

import net.i.xiangadmin.common.domain.ResponseDTO;
import net.i.xiangadmin.module.business.file.domain.vo.UploadVO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * [ 文件服务接口 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
public interface IFileService {


    /**
     * 文件上传
     *
     * @param multipartFile
     * @param path
     * @param fileType
     * @return
     */
    ResponseDTO<UploadVO> fileUpload(MultipartFile multipartFile, String path, Integer fileType, Long courseId);

    /**
     * 文件下载
     *
     * @param key
     * @param fileName
     * @param request
     * @return
     */
    ResponseEntity<byte[]> fileDownload(String key, String fileName, HttpServletRequest request);

    /**
     * 文件删除
     *
     * @param key
     * @param fileName
     * @param request
     * @return
     */
    ResponseDTO<String> fileDelete(String key, String fileName, HttpServletRequest request);



    /* 默认方法start：生成文件名、获取ContentType、下载具体操作 */

    /**
     * 生成文件名字（当前年月日时分秒 +32位 uuid + 文件格式后缀）
     *
     * @param originalFileName 原始文件名
     * @return String time + uuid + originalFileName
     */
    default String generateFileName(String originalFileName) {
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmms"));
        // String uuid = UUID.randomUUID().toString().replaceAll("-", "").substring(0,14);
        // String fileType = originalFileName.substring(originalFileName.lastIndexOf("."));
        return time + "-" + originalFileName;
    }

    /**
     * 根据文件拓展名，获取文件类型
     *
     * @param fileExt
     * @return
     */
    default String getContentType(String fileExt) {
        // 文件的后缀名
        if ("bmp".equalsIgnoreCase(fileExt)) {
            return "image/bmp";
        }
        if ("gif".equalsIgnoreCase(fileExt)) {
            return "image/gif";
        }
        if ("jpeg".equalsIgnoreCase(fileExt) || "jpg".equalsIgnoreCase(fileExt) || ".png".equalsIgnoreCase(fileExt)) {
            return "image/jpeg";
        }
        if ("png".equalsIgnoreCase(fileExt)) {
            return "image/png";
        }
        if ("html".equalsIgnoreCase(fileExt)) {
            return "text/html";
        }
        if ("txt".equalsIgnoreCase(fileExt) || "md".equalsIgnoreCase(fileExt)) {
            return "text/plain";
        }
        if ("vsd".equalsIgnoreCase(fileExt)) {
            return "application/vnd.visio";
        }
        if ("ppt".equalsIgnoreCase(fileExt) || "pptx".equalsIgnoreCase(fileExt)) {
            return "application/vnd.ms-powerpoint";
        }
        if ("doc".equalsIgnoreCase(fileExt) || "docx".equalsIgnoreCase(fileExt)) {
            return "application/msword";
        }
        if ("xml".equalsIgnoreCase(fileExt)) {
            return "text/xml";
        }
        return "";
    }

    default ResponseEntity<byte[]> downloadMethod(File file, HttpServletRequest request) {
        HttpHeaders heads = new HttpHeaders();
        heads.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream; charset=utf-8");
        String fileName = file.getName().replaceAll(" ","");
        try {
            if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
                // firefox浏览器
                //fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
                fileName = URLEncoder.encode(fileName, "UTF-8");
            } else if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
                // IE浏览器
                fileName = URLEncoder.encode(fileName, "UTF-8");
            } else if (request.getHeader("User-Agent").toUpperCase().indexOf("EDGE") > 0) {
                // WIN10浏览器
                fileName = URLEncoder.encode(fileName, "UTF-8");
            } else if (request.getHeader("User-Agent").toUpperCase().indexOf("CHROME") > 0) {
                // 谷歌
                // fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
                fileName = URLEncoder.encode(fileName, "UTF-8");
            } else {
                //万能乱码问题解决
                fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
            }
        } catch (UnsupportedEncodingException e) {
            // log.error("", e);
        }
        // URLEncoder加密会将+转换为%2B，@转换为%40，空格转换为+（刚开始对空格进行了替换）
        fileName = fileName.replace("%2B","+");
        fileName = fileName.replace("%40","@");
        heads.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
        try {
            InputStream in = new FileInputStream(file);
            // 输入流转换为字节流
            byte[] buffer = FileCopyUtils.copyToByteArray(in);
            ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(buffer, heads, HttpStatus.OK);
            //file.delete();
            return responseEntity;
        } catch (Exception e) {
            // log.error("", e);
        }
        return null;
    }

    /* 默认方法end：生成文件名、获取ContentType、下载具体操作 */
}

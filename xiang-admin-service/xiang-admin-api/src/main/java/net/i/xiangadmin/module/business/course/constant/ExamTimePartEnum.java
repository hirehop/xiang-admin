package net.i.xiangadmin.module.business.course.constant;

import net.i.xiangadmin.common.constant.BaseEnum;

public enum ExamTimePartEnum implements BaseEnum {
    One(1,"一二节课"),
    TWO(2,"三四节课"),
    THREE(3,"五六节课"),
    FOUR(4,"七八节课"),
    FIVE(5,"九十节课");

    ExamTimePartEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private Integer value;
    private String desc;

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}

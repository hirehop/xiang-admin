package net.i.xiangadmin.module.business.file.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.i.xiangadmin.common.domain.page.PageParamDTO;

/**
 * [ 文件查询DTO：文件名称、文件类型、开始时间、结束时间 ]
 * [ 继承至分页参数DTO ]
 */
@Data
public class FileQueryDTO extends PageParamDTO {

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    /**
     * 接受的是1/2/3，会被转换为相应type对应的desc
     */
    @ApiModelProperty(value = "文件类型")
    private String fileType;

    @ApiModelProperty("所属课程ID")
    private Long courseId;

    @ApiModelProperty("开始日期")
    private String startDate;

    @ApiModelProperty("结束日期")
    private String endDate;
}

package net.i.xiangadmin.common.domain.page;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * [ 排序基础类 = 列名 + ASC（默认true） ]
 *
 */

@Slf4j
@Data
public class OrderItemDTO {
    private String column;
    private boolean asc = true;
}

package net.i.xiangadmin.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * [ 不需要权限验证 ]
 *
 * @version 1.0
 * @since JDK1.8
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface NoValidPrivilege {

}
